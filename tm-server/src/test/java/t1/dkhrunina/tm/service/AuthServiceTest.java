package t1.dkhrunina.tm.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import t1.dkhrunina.tm.api.repository.IProjectRepository;
import t1.dkhrunina.tm.api.repository.ISessionRepository;
import t1.dkhrunina.tm.api.repository.ITaskRepository;
import t1.dkhrunina.tm.api.repository.IUserRepository;
import t1.dkhrunina.tm.api.service.*;
import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.exception.field.LoginEmptyException;
import t1.dkhrunina.tm.exception.field.PasswordEmptyException;
import t1.dkhrunina.tm.exception.user.AccessDeniedException;
import t1.dkhrunina.tm.model.Session;
import t1.dkhrunina.tm.model.User;
import t1.dkhrunina.tm.repository.ProjectRepository;
import t1.dkhrunina.tm.repository.SessionRepository;
import t1.dkhrunina.tm.repository.TaskRepository;
import t1.dkhrunina.tm.repository.UserRepository;
import t1.dkhrunina.tm.util.CryptUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AuthServiceTest {

    @NotNull
    private IAuthService authService;

    @NotNull
    private ISessionService sessionService;

    @NotNull
    private IUserService userService;

    @NotNull
    private IPropertyService propertyService;

    @NotNull
    private List<User> userList;

    @NotNull
    private User user;

    @Before
    public void initService() {
        @NotNull final IProjectRepository projectRepository = new ProjectRepository();
        @NotNull final IUserRepository userRepository = new UserRepository();
        @NotNull final ITaskRepository taskRepository = new TaskRepository();
        propertyService = new PropertyService();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository();
        @NotNull final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
        userService = new UserService(propertyService, userRepository, projectTaskService);
        sessionService = new SessionService(sessionRepository);
        authService = new AuthService(propertyService, userService, sessionService);
        @NotNull final User test = userService.create("Test", "Test", "test@email.com");
        @NotNull final User admin = userService.create("Admin", "Admin", Role.ADMIN);
        user = userService.create("User", "User", Role.ADMIN);
        userList = new ArrayList<>();
        userList.addAll(userService.findAll());
        @NotNull final Session session1 = new Session();
        session1.setUserId(test.getId());
        session1.setRole(test.getRole());
        @NotNull final Session session2 = new Session();
        session2.setUserId(admin.getId());
        session2.setRole(admin.getRole());
        @NotNull final Session session3 = new Session();
        session3.setUserId(user.getId());
        session3.setRole(user.getRole());
        sessionService.add(session1);
        sessionService.add(session2);
        sessionService.add(session3);
    }

    @Test
    public void testLogin() {
        Assert.assertTrue(userList.size() > 0);
        for (@NotNull final User user : userList) {
            @Nullable final String token = authService.login(user.getLogin(), user.getLogin());
            Assert.assertNotNull(token);
            Assert.assertFalse(token.isEmpty());
        }
    }

    @Test
    public void testLoginLoginEmpty() {
        Assert.assertThrows(LoginEmptyException.class, () -> authService.login("", user.getLogin()));
    }

    @Test
    public void testLoginLoginNull() {
        Assert.assertThrows(LoginEmptyException.class, () -> authService.login(null, user.getLogin()));
    }

    @Test
    public void testLoginUserNotFound() {
        Assert.assertThrows(AccessDeniedException.class, () -> authService.login("NewUser", "Pass"));
    }

    @Test
    public void testLoginPassEmpty() {
        Assert.assertThrows(PasswordEmptyException.class, () -> authService.login(user.getLogin(), ""));
    }

    @Test
    public void testLoginPassNull() {
        Assert.assertThrows(PasswordEmptyException.class, () -> authService.login(user.getLogin(), null));
    }

    @Test
    public void testLoginPassWrong() {
        Assert.assertThrows(AccessDeniedException.class, () -> authService.login(user.getLogin(), "WrongPass"));
    }

    @Test
    public void testLoginUserLocked() {
        user.setLocked(true);
        Assert.assertThrows(AccessDeniedException.class, () -> authService.login(user.getLogin(), user.getLogin()));
    }

    @Test
    public void testLogout() {
        Assert.assertTrue(userList.size() > 0);
        for (@NotNull final User user : userList) {
            @Nullable final String token = authService.login(user.getLogin(), user.getLogin());
            Assert.assertNotNull(token);
            Assert.assertFalse(token.isEmpty());
            @Nullable final Session validSession = authService.validateToken(token);
            Assert.assertNotNull(validSession);
            boolean isSessionExists = sessionService.existsById(validSession.getId());
            Assert.assertTrue(isSessionExists);
            authService.logout(token);
            isSessionExists = sessionService.existsById(validSession.getId());
            Assert.assertFalse(isSessionExists);
        }
    }

    @Test
    public void testLogoutTokenNull() {
        Assert.assertThrows(AccessDeniedException.class, () -> authService.logout(null));
    }

    @Test
    public void testLogoutTokenInvalid() {
        Assert.assertThrows(AccessDeniedException.class, () -> authService.logout("meow"));
    }

    @Test
    public void testValidateToken() {
        Assert.assertTrue(userList.size() > 0);
        for (@NotNull final User user : userList) {
            @Nullable final String token = authService.login(user.getLogin(), user.getLogin());
            Assert.assertNotNull(token);
            Assert.assertFalse(token.isEmpty());
            @NotNull final Session validSession = authService.validateToken(token);
            Assert.assertNotNull(validSession);
            boolean isSessionExists = sessionService.existsById(validSession.getId());
            Assert.assertTrue(isSessionExists);
        }
    }

    @Test
    public void testValidateTokenNull() {
        Assert.assertThrows(AccessDeniedException.class, () -> authService.validateToken(null));
    }

    @Test
    public void testValidateTokenInvalid() {
        Assert.assertThrows(AccessDeniedException.class, () -> authService.validateToken("meow"));
    }

    @Test
    public void testValidateTokenExpiredSession() throws JsonProcessingException {
        @Nullable final String token = authService.login(user.getLogin(), user.getLogin());
        Assert.assertNotNull(token);
        Assert.assertFalse(token.isEmpty());
        @NotNull final Session validSession = authService.validateToken(token);
        Assert.assertNotNull(validSession);
        @NotNull final Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, -30);
        @NotNull final Date dateBefore = cal.getTime();
        validSession.setDate(dateBefore);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String tokenForMapper = objectMapper.writeValueAsString(validSession);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @NotNull final String invalidToken = CryptUtil.encrypt(sessionKey, tokenForMapper);
        Assert.assertThrows(AccessDeniedException.class, () -> authService.validateToken(invalidToken));
    }

    @Test
    public void testValidateTokenSessionNull() {
        @Nullable final String token = authService.login(user.getLogin(), user.getLogin());
        Assert.assertNotNull(token);
        Assert.assertFalse(token.isEmpty());
        @NotNull final Session validSession = authService.validateToken(token);
        Assert.assertNotNull(validSession);
        @NotNull final String sessionId = validSession.getId();
        sessionService.removeById(sessionId);
        Assert.assertThrows(AccessDeniedException.class, () -> authService.validateToken(token));
    }

    @Test
    public void testInvalidate() {
        Assert.assertTrue(sessionService.getSize() > 0);
        @NotNull final List<Session> sessionList = sessionService.findAll();
        for (@NotNull final Session session : sessionList) {
            authService.invalidate(session);
        }
        Assert.assertEquals(0, sessionService.getSize());
    }

    @Test
    public void testInvalidateNull() {
        int expectedNumberOfEntries = sessionService.getSize();
        authService.invalidate(null);
        int actualNumberOfEntries = sessionService.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

    @Test
    public void testRegister() {
        @NotNull final String login = "LOGIN";
        @NotNull final String password = "PASSWORD";
        @NotNull final String email = "EMAIL";
        @NotNull final User user = authService.register(login, password, email);
        Assert.assertNotNull(user);
        @Nullable final User newUser = userService.findOneById(user.getId());
        Assert.assertNotNull(newUser);
        Assert.assertEquals(login, newUser.getLogin());
        Assert.assertEquals(email, newUser.getEmail());
        @Nullable final String token = authService.login(newUser.getLogin(), "PASSWORD");
        Assert.assertNotNull(token);
        Assert.assertFalse(token.isEmpty());
    }

}