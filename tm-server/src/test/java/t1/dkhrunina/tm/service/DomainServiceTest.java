package t1.dkhrunina.tm.service;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import t1.dkhrunina.tm.api.repository.IProjectRepository;
import t1.dkhrunina.tm.api.repository.ISessionRepository;
import t1.dkhrunina.tm.api.repository.ITaskRepository;
import t1.dkhrunina.tm.api.repository.IUserRepository;
import t1.dkhrunina.tm.api.service.*;
import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.model.Project;
import t1.dkhrunina.tm.model.Task;
import t1.dkhrunina.tm.model.User;
import t1.dkhrunina.tm.repository.ProjectRepository;
import t1.dkhrunina.tm.repository.SessionRepository;
import t1.dkhrunina.tm.repository.TaskRepository;
import t1.dkhrunina.tm.repository.UserRepository;

import java.util.List;

public class DomainServiceTest {

    @NotNull
    private IServiceLocator serviceLocator;

    @Before
    public void initService() {
        serviceLocator = new IServiceLocator() {

            @NotNull
            final IProjectRepository projectRepository = new ProjectRepository();

            @NotNull
            final IUserRepository userRepository = new UserRepository();

            @NotNull
            final ITaskRepository taskRepository = new TaskRepository();

            @NotNull
            final ISessionRepository sessionRepository = new SessionRepository();

            @Getter
            @NotNull
            final IPropertyService propertyService = new PropertyService();

            @Getter
            @NotNull
            final IProjectService projectService = new ProjectService(projectRepository);

            @Getter
            @NotNull
            final ITaskService taskService = new TaskService(taskRepository);

            @Getter
            @NotNull
            final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

            @Getter
            @NotNull
            final IUserService userService = new UserService(propertyService, userRepository, projectTaskService);

            @Getter
            @NotNull
            final IDomainService domainService = new DomainService(this);

            @Getter
            @NotNull
            final ISessionService sessionService = new SessionService(sessionRepository);

            @Getter
            @NotNull
            final IAuthService authService = new AuthService(propertyService, userService, sessionService);

            @Getter
            @NotNull
            final ILoggerService loggerService = new LoggerService(propertyService);
        };

        @NotNull final User user = serviceLocator.getUserService().create("USER", "USER", "user@user.user");
        @NotNull final User admin = serviceLocator.getUserService().create("ADMIN", "ADMIN", Role.ADMIN);
        @NotNull final Project project1 = serviceLocator.getProjectService().create(user.getId(), "project 1", "user project 1");
        @NotNull final Project project2 = serviceLocator.getProjectService().create(user.getId(), "project 2", "user project 2");
        @NotNull final Project project3 = serviceLocator.getProjectService().create(admin.getId(), "project 3", "admin project");
        @NotNull final Task task1 = serviceLocator.getTaskService().create(user.getId(), "task 1", "project 1 task");
        task1.setProjectId(project1.getId());
        @NotNull final Task task2 = serviceLocator.getTaskService().create(user.getId(), "task 2", "project 2 task");
        task2.setProjectId(project2.getId());
        @NotNull final Task task3 = serviceLocator.getTaskService().create(user.getId(), "task 3", "project 1 task");
        task3.setProjectId(project1.getId());
        @NotNull final Task task4 = serviceLocator.getTaskService().create(admin.getId(), "task 4", "project 3 task");
        task4.setProjectId(project3.getId());
    }

    @Test
    public void testDataBackup() {
        @NotNull final List<Project> projects = serviceLocator.getProjectService().findAll();
        Assert.assertTrue(projects.size() > 0);
        @NotNull final List<Task> tasks = serviceLocator.getTaskService().findAll();
        Assert.assertTrue(tasks.size() > 0);
        @NotNull final List<User> users = serviceLocator.getUserService().findAll();
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataBackup();
        serviceLocator.getProjectService().clear();
        serviceLocator.getTaskService().clear();
        serviceLocator.getUserService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectService().getSize());
        Assert.assertEquals(0, serviceLocator.getTaskService().getSize());
        Assert.assertEquals(0, serviceLocator.getUserService().getSize());
        serviceLocator.getDomainService().loadDataBackup();
        Assert.assertEquals(projects, serviceLocator.getProjectService().findAll());
        Assert.assertEquals(tasks, serviceLocator.getTaskService().findAll());
        Assert.assertEquals(users, serviceLocator.getUserService().findAll());
    }

    @Test
    public void testDataBase64() {
        @NotNull final List<Project> projects = serviceLocator.getProjectService().findAll();
        Assert.assertTrue(projects.size() > 0);
        @NotNull final List<Task> tasks = serviceLocator.getTaskService().findAll();
        Assert.assertTrue(tasks.size() > 0);
        @NotNull final List<User> users = serviceLocator.getUserService().findAll();
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataBase64();
        serviceLocator.getProjectService().clear();
        serviceLocator.getTaskService().clear();
        serviceLocator.getUserService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectService().getSize());
        Assert.assertEquals(0, serviceLocator.getTaskService().getSize());
        Assert.assertEquals(0, serviceLocator.getUserService().getSize());
        serviceLocator.getDomainService().loadDataBase64();
        Assert.assertEquals(projects, serviceLocator.getProjectService().findAll());
        Assert.assertEquals(tasks, serviceLocator.getTaskService().findAll());
        Assert.assertEquals(users, serviceLocator.getUserService().findAll());
    }

    @Test
    public void testDataBinary() {
        @NotNull final List<Project> projects = serviceLocator.getProjectService().findAll();
        Assert.assertTrue(projects.size() > 0);
        @NotNull final List<Task> tasks = serviceLocator.getTaskService().findAll();
        Assert.assertTrue(tasks.size() > 0);
        @NotNull final List<User> users = serviceLocator.getUserService().findAll();
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataBinary();
        serviceLocator.getProjectService().clear();
        serviceLocator.getTaskService().clear();
        serviceLocator.getUserService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectService().getSize());
        Assert.assertEquals(0, serviceLocator.getTaskService().getSize());
        Assert.assertEquals(0, serviceLocator.getUserService().getSize());
        serviceLocator.getDomainService().loadDataBinary();
        Assert.assertEquals(projects, serviceLocator.getProjectService().findAll());
        Assert.assertEquals(tasks, serviceLocator.getTaskService().findAll());
        Assert.assertEquals(users, serviceLocator.getUserService().findAll());
    }

    @Test
    public void testDataJsonFasterXml() {
        @NotNull final List<Project> projects = serviceLocator.getProjectService().findAll();
        Assert.assertTrue(projects.size() > 0);
        @NotNull final List<Task> tasks = serviceLocator.getTaskService().findAll();
        Assert.assertTrue(tasks.size() > 0);
        @NotNull final List<User> users = serviceLocator.getUserService().findAll();
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataJsonFasterXml();
        serviceLocator.getProjectService().clear();
        serviceLocator.getTaskService().clear();
        serviceLocator.getUserService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectService().getSize());
        Assert.assertEquals(0, serviceLocator.getTaskService().getSize());
        Assert.assertEquals(0, serviceLocator.getUserService().getSize());
        serviceLocator.getDomainService().loadDataJsonFasterXml();
        Assert.assertEquals(projects, serviceLocator.getProjectService().findAll());
        Assert.assertEquals(tasks, serviceLocator.getTaskService().findAll());
        Assert.assertEquals(users, serviceLocator.getUserService().findAll());
    }

    @Test
    public void testDataJsonJaxB() {
        @NotNull final List<Project> projects = serviceLocator.getProjectService().findAll();
        Assert.assertTrue(projects.size() > 0);
        @NotNull final List<Task> tasks = serviceLocator.getTaskService().findAll();
        Assert.assertTrue(tasks.size() > 0);
        @NotNull final List<User> users = serviceLocator.getUserService().findAll();
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataJsonJaxB();
        serviceLocator.getProjectService().clear();
        serviceLocator.getTaskService().clear();
        serviceLocator.getUserService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectService().getSize());
        Assert.assertEquals(0, serviceLocator.getTaskService().getSize());
        Assert.assertEquals(0, serviceLocator.getUserService().getSize());
        serviceLocator.getDomainService().loadDataJsonJaxB();
        Assert.assertEquals(projects, serviceLocator.getProjectService().findAll());
        Assert.assertEquals(tasks, serviceLocator.getTaskService().findAll());
        Assert.assertEquals(users, serviceLocator.getUserService().findAll());
    }

    @Test
    public void testDataXmlFasterXml() {
        @NotNull final List<Project> projects = serviceLocator.getProjectService().findAll();
        Assert.assertTrue(projects.size() > 0);
        @NotNull final List<Task> tasks = serviceLocator.getTaskService().findAll();
        Assert.assertTrue(tasks.size() > 0);
        @NotNull final List<User> users = serviceLocator.getUserService().findAll();
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataXmlFasterXml();
        serviceLocator.getProjectService().clear();
        serviceLocator.getTaskService().clear();
        serviceLocator.getUserService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectService().getSize());
        Assert.assertEquals(0, serviceLocator.getTaskService().getSize());
        Assert.assertEquals(0, serviceLocator.getUserService().getSize());
        serviceLocator.getDomainService().loadDataXmlFasterXml();
        Assert.assertEquals(projects, serviceLocator.getProjectService().findAll());
        Assert.assertEquals(tasks, serviceLocator.getTaskService().findAll());
        Assert.assertEquals(users, serviceLocator.getUserService().findAll());
    }

    @Test
    public void testDataXmlJaxB() {
        @NotNull final List<Project> projects = serviceLocator.getProjectService().findAll();
        Assert.assertTrue(projects.size() > 0);
        @NotNull final List<Task> tasks = serviceLocator.getTaskService().findAll();
        Assert.assertTrue(tasks.size() > 0);
        @NotNull final List<User> users = serviceLocator.getUserService().findAll();
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataXmlJaxB();
        serviceLocator.getProjectService().clear();
        serviceLocator.getTaskService().clear();
        serviceLocator.getUserService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectService().getSize());
        Assert.assertEquals(0, serviceLocator.getTaskService().getSize());
        Assert.assertEquals(0, serviceLocator.getUserService().getSize());
        serviceLocator.getDomainService().loadDataXmlJaxB();
        Assert.assertEquals(projects, serviceLocator.getProjectService().findAll());
        Assert.assertEquals(tasks, serviceLocator.getTaskService().findAll());
        Assert.assertEquals(users, serviceLocator.getUserService().findAll());
    }

    @Test
    public void testDataYamlFasterXml() {
        @NotNull final List<Project> projects = serviceLocator.getProjectService().findAll();
        Assert.assertTrue(projects.size() > 0);
        @NotNull final List<Task> tasks = serviceLocator.getTaskService().findAll();
        Assert.assertTrue(tasks.size() > 0);
        @NotNull final List<User> users = serviceLocator.getUserService().findAll();
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataYamlFasterXml();
        serviceLocator.getProjectService().clear();
        serviceLocator.getTaskService().clear();
        serviceLocator.getUserService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectService().getSize());
        Assert.assertEquals(0, serviceLocator.getTaskService().getSize());
        Assert.assertEquals(0, serviceLocator.getUserService().getSize());
        serviceLocator.getDomainService().loadDataYamlFasterXml();
        Assert.assertEquals(projects, serviceLocator.getProjectService().findAll());
        Assert.assertEquals(tasks, serviceLocator.getTaskService().findAll());
        Assert.assertEquals(users, serviceLocator.getUserService().findAll());
    }

}