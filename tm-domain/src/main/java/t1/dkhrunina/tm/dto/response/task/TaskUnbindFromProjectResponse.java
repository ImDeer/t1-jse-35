package t1.dkhrunina.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.dto.response.AbstractResultResponse;
import t1.dkhrunina.tm.model.Task;

@Getter
@Setter
@NoArgsConstructor
public class TaskUnbindFromProjectResponse extends AbstractResultResponse {

    @NotNull
    private Task task;

    public TaskUnbindFromProjectResponse(@NotNull final Task task) {
        this.task = task;
    }

}