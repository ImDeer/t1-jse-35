package t1.dkhrunina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.request.task.TaskChangeStatusByIdRequest;
import t1.dkhrunina.tm.enumerated.Status;
import t1.dkhrunina.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskChangeStatusByIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "t-change-status-by-id";

    @NotNull
    private static final String DESCRIPTION = "Change task status by id.";

    @Override
    public void execute() {
        System.out.println("[Change task status by id]");
        System.out.println("Enter id: ");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("Enter status: ");
        System.out.println(Arrays.toString(Status.toValues()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @Nullable final Status status = Status.toStatus(statusValue);
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(getToken(), id, status);
        getTaskEndpoint().changeTaskStatusById(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}