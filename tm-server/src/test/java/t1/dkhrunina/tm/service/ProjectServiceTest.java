package t1.dkhrunina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import t1.dkhrunina.tm.api.repository.IProjectRepository;
import t1.dkhrunina.tm.api.repository.ITaskRepository;
import t1.dkhrunina.tm.api.repository.IUserRepository;
import t1.dkhrunina.tm.api.service.*;
import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.enumerated.Sort;
import t1.dkhrunina.tm.enumerated.Status;
import t1.dkhrunina.tm.exception.entity.EntityNotFoundException;
import t1.dkhrunina.tm.exception.entity.ProjectNotFoundException;
import t1.dkhrunina.tm.exception.field.*;
import t1.dkhrunina.tm.model.Project;
import t1.dkhrunina.tm.model.Task;
import t1.dkhrunina.tm.model.User;
import t1.dkhrunina.tm.repository.ProjectRepository;
import t1.dkhrunina.tm.repository.TaskRepository;
import t1.dkhrunina.tm.repository.UserRepository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ProjectServiceTest {

    @NotNull
    private List<Project> projectList;

    @NotNull
    private IProjectService projectService;

    @NotNull
    private User user;

    @NotNull
    private User admin;

    @Before
    public void initService() {
        @NotNull final IProjectRepository projectRepository = new ProjectRepository();
        @NotNull final IUserRepository userRepository = new UserRepository();
        @NotNull final ITaskRepository taskRepository = new TaskRepository();
        @NotNull final IPropertyService propertyService = new PropertyService();
        projectService = new ProjectService(projectRepository);
        @NotNull final ITaskService taskService = new TaskService(taskRepository);
        @NotNull final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
        @NotNull final IUserService userService = new UserService(propertyService, userRepository, projectTaskService);
        projectList = new ArrayList<>();
        user = userService.create("USER", "USER", "user@user.user");
        admin = userService.create("ADMIN", "ADMIN", Role.ADMIN);
        @NotNull final Project project1 = projectService.create(user.getId(), "project 1", "user project 1");
        @NotNull final Project project2 = projectService.create(user.getId(), "project 2", "user project 2");
        @NotNull final Project project3 = projectService.create(admin.getId(), "project 3", "admin project");
        @NotNull final Task task1 = taskService.create(user.getId(), "task 1", "project 1 task");
        task1.setProjectId(project1.getId());
        @NotNull final Task task2 = taskService.create(user.getId(), "task 2", "project 2 task");
        task2.setProjectId(project2.getId());
        @NotNull final Task task3 = taskService.create(user.getId(), "task 3", "project 1 task");
        task3.setProjectId(project1.getId());
        @NotNull final Task task4 = taskService.create(admin.getId(), "task 4", "project 3 task");
        task4.setProjectId(project3.getId());
        projectList.add(project1);
        projectList.add(project2);
        projectList.add(project3);
    }

    @Test
    public void testAdd() {
        int expectedNumberOfEntries = projectService.getSize() + 1;
        @NotNull final Project project = new Project();
        project.setUserId("45");
        project.setName("Test Add");
        project.setDescription("Test Add");
        projectService.add(project);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void testAddForUser() {
        int expectedNumberOfEntries = projectService.getSize(user.getId()) + 1;
        @NotNull final Project project = new Project();
        project.setUserId(user.getId());
        project.setName("Test Add");
        project.setDescription("Test Add");
        projectService.add(user.getId(), project);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize(user.getId()));
    }

    @Test
    public void testAddNullForUser() {
        int expectedNumberOfEntries = projectService.getSize(user.getId());
        @Nullable final Project project = projectService.add(user.getId(), null);
        Assert.assertNull(project);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize(user.getId()));
    }

    @Test
    public void testAddCollection() {
        int expectedNumberOfEntries = projectService.getSize() + 2;
        @NotNull final List<Project> projectList = new ArrayList<>();
        @NotNull final Project firstProject = new Project();
        firstProject.setUserId("45");
        firstProject.setName("Test Name 1");
        firstProject.setDescription("Test Descr 1");
        projectList.add(firstProject);
        @NotNull final Project secondProject = new Project();
        secondProject.setUserId("45");
        secondProject.setName("Test Name 2");
        secondProject.setDescription("Test Descr 2");
        projectList.add(secondProject);
        projectService.add(projectList);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void testChangeProjectStatusById() {
        @NotNull final List<Project> projects = projectService.findAll(user.getId());
        for (@NotNull final Project project : projects) {
            @NotNull final String projectId = project.getId();
            @Nullable Project changedProject = projectService.changeProjectStatusById(user.getId(), projectId, Status.IN_PROGRESS);
            Assert.assertNotNull(changedProject);
            changedProject = projectService.findOneById(projectId);
            Assert.assertNotNull(changedProject);
            Assert.assertEquals(Status.IN_PROGRESS, changedProject.getStatus());
        }
    }

    @Test
    public void testChangeProjectStatusByIdEmpty() {
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectService.changeProjectStatusById(user.getId(), "", Status.IN_PROGRESS));
    }

    @Test
    public void testChangeProjectStatusByIdNull() {
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectService.changeProjectStatusById(user.getId(), null, Status.IN_PROGRESS));
    }

    @Test
    public void testChangeProjectStatusByIdProjectInvalid() {
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.changeProjectStatusById(user.getId(), "123", Status.IN_PROGRESS));
    }

    @Test
    public void testChangeProjectStatusByIdStatusIncorrect() {
        Assert.assertThrows(StatusIncorrectException.class, () -> projectService.changeProjectStatusById(user.getId(), "123", null));
    }

    @Test
    public void testChangeProjectStatusByIndex() {
        @NotNull final List<Project> projects = projectService.findAll(user.getId());
        for (int i = 0; i < projects.size(); i++) {
            @NotNull final Project project = projects.get(i);
            @NotNull final String projectId = project.getId();
            @Nullable Project changedProject = projectService.changeProjectStatusByIndex(user.getId(), i, Status.IN_PROGRESS);
            Assert.assertNotNull(changedProject);
            changedProject = projectService.findOneById(projectId);
            Assert.assertNotNull(changedProject);
            Assert.assertEquals(Status.IN_PROGRESS, changedProject.getStatus());
        }
    }

    @Test
    public void testChangeProjectStatusByIndexNull() {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.changeProjectStatusByIndex(user.getId(), null, Status.IN_PROGRESS));
    }

    @Test
    public void testChangeProjectStatusByIndexNegative() {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.changeProjectStatusByIndex(user.getId(), -1, Status.IN_PROGRESS));
    }

    @Test
    public void testChangeProjectStatusByIndexInvalid() {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.changeProjectStatusByIndex(user.getId(), 100, Status.IN_PROGRESS));
    }

    @Test
    public void testChangeProjectStatusByIndexStatusIncorrect() {
        Assert.assertThrows(StatusIncorrectException.class, () -> projectService.changeProjectStatusByIndex(user.getId(), 0, null));
    }

    @Test
    public void testClear() {
        int expectedNumberOfEntries = 0;
        Assert.assertTrue(projectService.getSize() > 0);
        projectService.clear();
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void testClearForUser() {
        int expectedNumberOfEntries = 0;
        Assert.assertTrue(projectService.getSize(user.getId()) > 0);
        projectService.clear(user.getId());
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize(user.getId()));
    }

    @Test
    public void testCreate() {
        int expectedNumberOfEntries = projectService.getSize(user.getId()) + 1;
        @NotNull final String name = "Project Name";
        @NotNull final String description = "Project Description";
        @Nullable Project createdProject = projectService.create(user.getId(), name, description);
        @NotNull final String projectId = createdProject.getId();
        Assert.assertNotNull(createdProject);
        createdProject = projectService.findOneById(user.getId(), projectId);
        Assert.assertNotNull(createdProject);
        Assert.assertEquals(name, createdProject.getName());
        Assert.assertEquals(description, createdProject.getDescription());
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize(user.getId()));
    }

    @Test
    public void testCreateByName() {
        int expectedNumberOfEntries = projectService.getSize(user.getId()) + 1;
        @NotNull final String name = "Project name";
        @Nullable Project createdProject = projectService.create(user.getId(), name);
        @NotNull final String projectId = createdProject.getId();
        Assert.assertNotNull(createdProject);
        createdProject = projectService.findOneById(user.getId(), projectId);
        Assert.assertNotNull(createdProject);
        Assert.assertEquals(name, createdProject.getName());
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize(user.getId()));
    }

    @Test
    public void testCreateNameEmpty() {
        Assert.assertThrows(NameEmptyException.class, () -> projectService.create(user.getId(), "", "description"));
    }

    @Test
    public void testCreateNameNull() {
        Assert.assertThrows(NameEmptyException.class, () -> projectService.create(user.getId(), null, "description"));
    }

    @Test
    public void testCreateDescriptionEmpty() {
        Assert.assertThrows(DescriptionEmptyException.class, () -> projectService.create(user.getId(), "name", ""));
    }

    @Test
    public void testCreateDescriptionNull() {
        Assert.assertThrows(DescriptionEmptyException.class, () -> projectService.create(user.getId(), "name", null));
    }

    @Test
    public void testCreateNameEmptyDescriptionNull() {
        Assert.assertThrows(NameEmptyException.class, () -> projectService.create(user.getId(), ""));
    }

    @Test
    public void testCreateByNameNullDescriptionNull() {
        Assert.assertThrows(NameEmptyException.class, () -> projectService.create(user.getId(), null));
    }

    @Test
    public void testSet() {
        int expectedNumberOfEntries = 2;
        @NotNull final List<Project> projectList = new ArrayList<>();
        @NotNull final Project firstProject = new Project();
        firstProject.setUserId("45");
        firstProject.setName("Test Name 1");
        firstProject.setDescription("Test Description 1");
        projectList.add(firstProject);
        @NotNull final Project secondProject = new Project();
        secondProject.setUserId("45");
        secondProject.setName("Test Name 2");
        secondProject.setDescription("Test Description 2");
        projectList.add(secondProject);
        projectService.set(projectList);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void testExistById() {
        for (@NotNull final Project project : projectList) {
            final boolean isExist = projectService.existsById(project.getId());
            Assert.assertTrue(isExist);
        }
    }

    @Test
    public void testExistByIdInvalid() {
        final boolean isExist = projectService.existsById("123321");
        Assert.assertFalse(isExist);
    }

    @Test
    public void testExistByIdForUser() {
        @NotNull final List<Project> projectsForTestUser = projectList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final Project project : projectsForTestUser) {
            final boolean isExist = projectService.existsById(user.getId(), project.getId());
            Assert.assertTrue(isExist);
        }
    }

    @Test
    public void testExistByIdForUserInvalid() {
        final boolean isExist = projectService.existsById("45", "123321");
        Assert.assertFalse(isExist);
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Project> projects = projectService.findAll();
        Assert.assertEquals(projectList, projects);
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<Project> projectsForTestUser = projectList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull final List<Project> projects = projectService.findAll(user.getId());
        Assert.assertEquals(projectsForTestUser, projects);
    }

    @Test
    public void findAllWithComparatorTest() {
        @Nullable Comparator<Project> comparator = Sort.BY_NAME.getComparator();
        @NotNull List<Project> projects = projectService.findAll(comparator);
        Assert.assertEquals(projectList, projects);
        comparator = Sort.BY_CREATED.getComparator();
        projects = projectService.findAll(comparator);
        Assert.assertEquals(projectList, projects);
        comparator = Sort.BY_STATUS.getComparator();
        projects = projectService.findAll(comparator);
        Assert.assertEquals(projectList, projects);
        comparator = null;
        projects = projectService.findAll(comparator);
        Assert.assertEquals(projectList, projects);
    }

    @Test
    public void testFindAllWithComparatorForUser() {
        @NotNull final List<Project> projectsForTestUser = projectList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        @Nullable Comparator<Project> comparator = Sort.BY_NAME.getComparator();
        @NotNull List<Project> projects = projectService.findAll(user.getId(), comparator);
        Assert.assertEquals(projectsForTestUser, projects);
        comparator = Sort.BY_CREATED.getComparator();
        projects = projectService.findAll(user.getId(), comparator);
        Assert.assertEquals(projectsForTestUser, projects);
        comparator = Sort.BY_STATUS.getComparator();
        projects = projectService.findAll(user.getId(), comparator);
        Assert.assertEquals(projectsForTestUser, projects);
    }

    @Test
    public void testFindAllWithSortForUser() {
        @NotNull final List<Project> projectsForTestUser = projectList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull List<Project> projects = projectService.findAll(user.getId(), Sort.BY_NAME);
        Assert.assertEquals(projectsForTestUser, projects);
        projects = projectService.findAll(user.getId(), Sort.BY_CREATED);
        Assert.assertEquals(projectsForTestUser, projects);
        projects = projectService.findAll(user.getId(), Sort.BY_STATUS);
        Assert.assertEquals(projectsForTestUser, projects);
        @Nullable final Sort sort = null;
        projects = projectService.findAll(user.getId(), sort);
        Assert.assertEquals(projectsForTestUser, projects);
    }

    @Test
    public void testFindOneById() {
        @Nullable Project foundProject;
        for (@NotNull final Project project : projectList) {
            foundProject = projectService.findOneById(project.getId());
            Assert.assertNotNull(foundProject);
        }
    }

    @Test
    public void testFindOneByIdNull() {
        @Nullable final Project foundProject = projectService.findOneById(null);
        Assert.assertNull(foundProject);
    }

    @Test
    public void testFindOneByIdEmpty() {
        @Nullable final Project foundProject = projectService.findOneById("");
        Assert.assertNull(foundProject);
    }

    @Test
    public void testFindOneByIdNullForUser() {
        @Nullable final Project foundProject = projectService.findOneById(user.getId(), null);
        Assert.assertNull(foundProject);
    }

    @Test
    public void testFindOneByIdForUser() {
        @Nullable Project foundProject;
        @NotNull final List<Project> projectsForTestUser = projectList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final Project project : projectsForTestUser) {
            foundProject = projectService.findOneById(user.getId(), project.getId());
            Assert.assertNotNull(foundProject);
        }
    }

    @Test
    public void testFindOneByIndex() {
        for (int i = 0; i < projectList.size(); i++) {
            @Nullable final Project project = projectService.findOneByIndex(i);
            Assert.assertNotNull(project);
        }
    }

    @Test
    public void testFindOneByIndexInvalid() {
        int index = projectService.getSize() + 1;
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.findOneByIndex(index));
    }

    @Test
    public void testFindOneByIndexNull() {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.findOneByIndex(null));
    }

    @Test
    public void testFindOneByIndexNegative() {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.findOneByIndex(-1));
    }

    @Test
    public void testFindOneByIndexForUser() {
        @NotNull final List<Project> projectsForTestUser = projectList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 0; i < projectsForTestUser.size(); i++) {
            @Nullable final Project project = projectService.findOneByIndex(user.getId(), i);
            Assert.assertNotNull(project);
        }
    }

    @Test
    public void testFindOneByIndexInvalidForUser() {
        int index = projectService.getSize(user.getId()) + 1;
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.findOneByIndex(user.getId(), index));
    }

    @Test
    public void testFindOneByIndexNullForUser() {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.findOneByIndex(user.getId(), null));
    }

    @Test
    public void testFindOneByIndexNegativeForUser() {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.findOneByIndex(user.getId(), -1));
    }

    @Test
    public void testGetSize() {
        int expectedSize = projectList.size();
        int actualSize = projectService.getSize();
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void testGetSizeForUser() {
        int expectedSize = (int) projectList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .count();
        int actualSize = projectService.getSize(user.getId());
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void testRemove() {
        for (@NotNull final Project project : projectList) {
            @NotNull final String projectId = project.getId();
            @Nullable final Project deletedProject = projectService.remove(project);
            Assert.assertNotNull(deletedProject);
            @Nullable final Project deletedProjectInRepository = projectService.findOneById(projectId);
            Assert.assertNull(deletedProjectInRepository);
        }
    }

    @Test
    public void testRemoveEntityNew() {
        @NotNull final Project project = new Project();
        Assert.assertThrows(EntityNotFoundException.class, () -> projectService.remove(project));
    }

    @Test
    public void testRemoveEntityNull() {
        Assert.assertThrows(EntityNotFoundException.class, () -> projectService.remove(null));
    }

    @Test
    public void testRemoveAll() {
        @NotNull final List<Project> projectsForTestUser = projectList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        projectService.removeAll(projectsForTestUser);
        Assert.assertEquals(0, projectService.getSize(user.getId()));
    }

    @Test
    public void testRemoveById() {
        for (@NotNull final Project project : projectList) {
            @NotNull final String projectId = project.getId();
            @Nullable final Project deletedProject = projectService.removeById(projectId);
            Assert.assertNotNull(deletedProject);
            @Nullable final Project deletedProjectInRepository = projectService.findOneById(projectId);
            Assert.assertNull(deletedProjectInRepository);
        }
    }

    @Test
    public void testRemoveByIdNull() {
        Assert.assertThrows(IdEmptyException.class, () -> projectService.removeById(null));
    }

    @Test
    public void testRemoveByIdEmpty() {
        Assert.assertThrows(IdEmptyException.class, () -> projectService.removeById(""));
    }

    @Test
    public void testRemoveByIdNotFound() {
        Assert.assertThrows(EntityNotFoundException.class, () -> projectService.removeById("123321"));
    }

    @Test
    public void testRemoveByIdForUser() {
        @NotNull final List<Project> projectsForTestUser = projectList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final Project project : projectsForTestUser) {
            @NotNull final String projectId = project.getId();
            @Nullable final Project deletedProject = projectService.removeById(user.getId(), projectId);
            Assert.assertNotNull(deletedProject);
            @Nullable final Project deletedProjectInRepository = projectService.findOneById(user.getId(), projectId);
            Assert.assertNull(deletedProjectInRepository);
        }
    }

    @Test
    public void testRemoveByIdNullForUser() {
        Assert.assertThrows(IdEmptyException.class, () -> projectService.removeById(user.getId(), null));
    }

    @Test
    public void testRemoveByIdEmptyForUser() {
        Assert.assertThrows(IdEmptyException.class, () -> projectService.removeById(user.getId(), ""));
    }

    @Test
    public void testRemoveByIdForUserNotFound() {
        Assert.assertThrows(EntityNotFoundException.class, () -> projectService.removeById(user.getId(), "123321"));
    }

    @Test
    public void testRemoveByIndex() {
        int index = projectList.size();
        while (index > 0) {
            @Nullable final Project deletedProject = projectService.removeByIndex(index - 1);
            Assert.assertNotNull(deletedProject);
            @NotNull final String projectId = deletedProject.getId();
            @Nullable final Project deletedProjectInRepository = projectService.findOneById(projectId);
            Assert.assertNull(deletedProjectInRepository);
            index--;
        }
    }

    @Test
    public void testRemoveByIndexIncorrect() {
        int index = projectList.size() + 1;
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.removeByIndex(index));
    }

    @Test
    public void testRemoveByIndexNull() {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.removeByIndex(null));
    }

    @Test
    public void testRemoveByIndexNegative() {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.removeByIndex(-1));
    }

    @Test
    public void testRemoveByIndexForUser() {
        int index = (int) projectList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .count();
        while (index > 0) {
            @Nullable final Project deletedProject = projectService.removeByIndex(user.getId(), index - 1);
            Assert.assertNotNull(deletedProject);
            @NotNull final String projectId = deletedProject.getId();
            @Nullable final Project deletedProjectInRepository = projectService.findOneById(user.getId(), projectId);
            Assert.assertNull(deletedProjectInRepository);
            index--;
        }
    }

    @Test
    public void testRemoveByIndexIncorrectForUser() {
        int index = projectList.size() + 1;
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.removeByIndex(user.getId(), index));
    }

    @Test
    public void testRemoveByIndexNullForUser() {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.removeByIndex(user.getId(), null));
    }

    @Test
    public void testRemoveByIndexNegativeForUser() {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.removeByIndex(user.getId(), -1));
    }

    @Test
    public void testUpdateById() {
        @NotNull final List<Project> projects = projectService.findAll(user.getId());
        @NotNull String name;
        @NotNull String description;
        int index = 0;
        for (@NotNull final Project project : projects) {
            @NotNull final String projectId = project.getId();
            name = "name " + index;
            description = "description" + index;
            @Nullable Project updatedProject = projectService.updateById(user.getId(), projectId, name, description);
            Assert.assertNotNull(updatedProject);
            updatedProject = projectService.findOneById(projectId);
            Assert.assertNotNull(updatedProject);
            Assert.assertEquals(name, updatedProject.getName());
            Assert.assertEquals(description, updatedProject.getDescription());
            index++;
        }
    }

    @Test
    public void testUpdateByIdEmpty() {
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.updateById(user.getId(), "", "name", "description"));
    }

    @Test
    public void testUpdateByIdNull() {
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.updateById(user.getId(), null, "name", "description"));
    }

    @Test
    public void testUpdateByIdNameEmpty() {
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateById(user.getId(), "id", "", "description"));
    }

    @Test
    public void testUpdateByIdNameNull() {
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateById(user.getId(), "id", null, "description"));
    }

    @Test
    public void testUpdateByIdProjectNotFound() {
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.updateById(user.getId(), "123", "name", "description"));
    }

    @Test
    public void testUpdateByIndex() {
        @NotNull final List<Project> projects = projectService.findAll(user.getId());
        @NotNull String name;
        @NotNull String description;
        int index = 0;
        for (@NotNull final Project project : projects) {
            @NotNull final String projectId = project.getId();
            name = "name " + index;
            description = "description" + index;
            @Nullable Project updatedProject = projectService.updateByIndex(user.getId(), index, name, description);
            Assert.assertNotNull(updatedProject);
            updatedProject = projectService.findOneById(projectId);
            Assert.assertNotNull(updatedProject);
            Assert.assertEquals(name, updatedProject.getName());
            Assert.assertEquals(description, updatedProject.getDescription());
            index++;
        }
    }

    @Test
    public void testUpdateByIndexNull() {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.updateByIndex(user.getId(), null, "name", "description"));
    }

    @Test
    public void testUpdateByIndexNegative() {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.updateByIndex(user.getId(), -1, "name", "description"));
    }

    @Test
    public void testUpdateByIndexIncorrect() {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.updateByIndex(user.getId(), 100, "name", "description"));
    }

    @Test
    public void testUpdateByIndexNameNull() {
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateByIndex(user.getId(), 0, null, "description"));
    }

    @Test
    public void testUpdateByIndexNameEmpty() {
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateByIndex(user.getId(), 0, "", "description"));
    }

}