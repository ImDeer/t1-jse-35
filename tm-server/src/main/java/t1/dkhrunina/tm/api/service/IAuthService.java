package t1.dkhrunina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.model.Session;
import t1.dkhrunina.tm.model.User;

public interface IAuthService {

    @NotNull
    String login(@Nullable String login, @Nullable String password);

    void logout(@Nullable String token);

    @NotNull
    Session validateToken(@Nullable String token);

    void invalidate(@Nullable Session session);

    @NotNull
    User register(@NotNull String login, @NotNull String password, @Nullable String email);

}