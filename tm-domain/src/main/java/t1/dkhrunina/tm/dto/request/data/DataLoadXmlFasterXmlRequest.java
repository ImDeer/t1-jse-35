package t1.dkhrunina.tm.dto.request.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class DataLoadXmlFasterXmlRequest extends AbstractUserRequest {

    public DataLoadXmlFasterXmlRequest(@Nullable final String token) {
        super(token);
    }

}