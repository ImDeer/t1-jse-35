package t1.dkhrunina.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}