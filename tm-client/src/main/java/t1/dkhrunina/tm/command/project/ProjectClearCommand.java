package t1.dkhrunina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.dto.request.project.ProjectClearRequest;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    private static final String NAME = "pr-clear";

    @NotNull
    private static final String DESCRIPTION = "Delete all projects.";

    @Override
    public void execute() {
        System.out.println("[Clear project list]");
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(getToken());
        getProjectEndpoint().clearProject(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}