package t1.dkhrunina.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.response.AbstractResponse;
import t1.dkhrunina.tm.model.User;

@Getter
@Setter
@NoArgsConstructor
public class UserChangePasswordResponse extends AbstractResponse {

    @Nullable
    private User user;

    public UserChangePasswordResponse(@Nullable final User user) {
        this.user = user;
    }

}