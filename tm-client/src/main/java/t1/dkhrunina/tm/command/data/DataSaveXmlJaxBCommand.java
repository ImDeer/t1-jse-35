package t1.dkhrunina.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.request.data.DataSaveXmlJaxBRequest;

public final class DataSaveXmlJaxBCommand extends AbstractDataCommand {

    @NotNull
    private static final String NAME = "data-s-xml-jaxb";

    @NotNull
    private static final String DESCRIPTION = "Save data to JaxB XML file.";

    @SneakyThrows
    @Override
    public void execute() {
        getDomainEndpoint().saveXmlJaxBData(new DataSaveXmlJaxBRequest(getToken()));
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}