package t1.dkhrunina.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.request.data.DataLoadBase64Request;

public final class DataLoadBase64Command extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-l-base64";

    @NotNull
    private static final String DESCRIPTION = "Load data from BASE64 file.";

    @SneakyThrows
    @Override
    public void execute() {
        getDomainEndpoint().loadBase64Data(new DataLoadBase64Request(getToken()));
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}