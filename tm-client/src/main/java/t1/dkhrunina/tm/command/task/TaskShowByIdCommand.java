package t1.dkhrunina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.request.task.TaskFindOneByIdRequest;
import t1.dkhrunina.tm.model.Task;
import t1.dkhrunina.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "t-show-by-id";

    @NotNull
    private static final String DESCRIPTION = "Show task by id.";

    @Override
    public void execute() {
        System.out.println("[Show task by id]");
        System.out.println("Enter id: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskFindOneByIdRequest request = new TaskFindOneByIdRequest(getToken(), id);
        @Nullable final Task task = getTaskEndpoint().findTaskById(request).getTask();
        showTask(task);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}