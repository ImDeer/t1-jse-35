package t1.dkhrunina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.request.user.UserShowProfileRequest;
import t1.dkhrunina.tm.dto.response.user.UserShowProfileResponse;
import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.model.User;

public final class UserShowProfileCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "u-show-profile";

    @NotNull
    private static final String DESCRIPTION = "Show current user profile.";

    @Override
    public void execute() {
        @NotNull final UserShowProfileRequest request = new UserShowProfileRequest(getToken());
        @NotNull final UserShowProfileResponse response = getAuthEndpoint().showUserProfile(request);
        @Nullable final User user = response.getUser();
        if (user != null) {
            System.out.println("[Show user profile]");
            System.out.println("Id: " + user.getId());
            System.out.println("Login: " + user.getLogin());
            System.out.println("First name: " + user.getFirstName());
            System.out.println("Middle name: " + user.getMiddleName());
            System.out.println("Last name: " + user.getLastName());
            System.out.println("Email: " + user.getEmail());
            System.out.println("Role: " + user.getRole().getDisplayName());
        }
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}