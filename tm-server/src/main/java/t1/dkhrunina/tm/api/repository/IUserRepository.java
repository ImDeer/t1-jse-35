package t1.dkhrunina.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    boolean isEmailExist(@Nullable String email);

    boolean isLoginExist(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    @Nullable
    User findById(@Nullable String id);

    @Nullable
    User findByLogin(@Nullable String login);

}