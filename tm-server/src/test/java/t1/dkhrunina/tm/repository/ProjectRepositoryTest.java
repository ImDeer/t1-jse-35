package t1.dkhrunina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import t1.dkhrunina.tm.api.repository.IProjectRepository;
import t1.dkhrunina.tm.enumerated.Sort;
import t1.dkhrunina.tm.exception.entity.EntityNotFoundException;
import t1.dkhrunina.tm.exception.field.IndexIncorrectException;
import t1.dkhrunina.tm.model.Project;
import t1.dkhrunina.tm.util.SystemUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ProjectRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private final String USER_ID_1 = SystemUtil.generateGuid();

    @NotNull
    private final String USER_ID_2 = SystemUtil.generateGuid();

    @NotNull
    private List<Project> projectList;

    @NotNull
    private IProjectRepository projectRepository;

    @Before
    public void initRepository() {
        projectList = new ArrayList<>();
        projectRepository = new ProjectRepository();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("project " + i);
            project.setDescription("description " + i);
            if (i <= 5) project.setUserId(USER_ID_1);
            else project.setUserId(USER_ID_2);
            projectRepository.add(project);
            projectList.add(project);
        }
    }

    @Test
    public void testAdd() {
        int expectedNumberOfEntries = projectRepository.getSize() + 1;
        @NotNull final String name = "Test Project";
        @NotNull final String description = "Test Description";
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(USER_ID_1, project);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
        @Nullable final Project createdProject = projectRepository.findOneByIndex(expectedNumberOfEntries - 1);
        Assert.assertNotNull(createdProject);
        Assert.assertEquals(USER_ID_1, createdProject.getUserId());
        Assert.assertEquals(name, createdProject.getName());
        Assert.assertEquals(description, createdProject.getDescription());
    }

    @Test
    public void testAddNull() {
        @Nullable final Project project = projectRepository.add(USER_ID_1, null);
        Assert.assertNull(project);
    }

    @Test
    public void testAddMultiple() {
        int expectedNumberOfEntries = projectRepository.getSize() + 2;
        @NotNull final List<Project> projects = new ArrayList<>();
        @NotNull final String firstProjectName = "Test Project 1";
        @NotNull final String firstProjectDescription = "Test Description 1";
        @NotNull final Project firstProject = new Project();
        firstProject.setName(firstProjectName);
        firstProject.setDescription(firstProjectDescription);
        firstProject.setUserId(USER_ID_1);
        projects.add(firstProject);
        @NotNull final String secondProjectName = "Test Project 2";
        @NotNull final String secondProjectDescription = "Test Description 2";
        @NotNull final Project secondProject = new Project();
        secondProject.setName(secondProjectName);
        secondProject.setDescription(secondProjectDescription);
        secondProject.setUserId(USER_ID_1);
        projects.add(secondProject);
        projectRepository.add(projects);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testClear() {
        int expectedNumberOfEntries = 0;
        projectRepository.clear();
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testClearForUser() {
        int expectedNumberOfEntries = 0;
        projectRepository.clear(USER_ID_1);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize(USER_ID_1));
    }

    @Test
    public void testExistByIdTrue() {
        @Nullable final Project project = projectRepository.findOneByIndex(1);
        Assert.assertNotNull(project);
        @Nullable final String projectId = project.getId();
        Assert.assertTrue(projectRepository.existsById(projectId));
    }

    @Test
    public void testExistByIdFalse() {
        Assert.assertFalse(projectRepository.existsById("123123"));
    }

    @Test
    public void testExistByIdForUserTrue() {
        @Nullable final Project project = projectRepository.findOneByIndex(USER_ID_1, 0);
        Assert.assertNotNull(project);
        @Nullable final String projectId = project.getId();
        Assert.assertTrue(projectRepository.existsById(USER_ID_1, projectId));
    }

    @Test
    public void testExistByIdForUserFalse() {
        Assert.assertFalse(projectRepository.existsById(USER_ID_1, "123123"));
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Project> projects = projectRepository.findAll();
        Assert.assertEquals(projectList, projects);
    }

    @Test
    @SuppressWarnings("unchecked assignment")
    public void testFindAllSortByName() {
        @NotNull Comparator<Project> comparator = Sort.BY_NAME.getComparator();
        @NotNull List<Project> projects = projectRepository.findAll(comparator);
        projectList.sort(comparator);
        Assert.assertEquals(projectList, projects);
    }

    @Test
    @SuppressWarnings("unchecked assignment")
    public void testFindAllSortByCreated() {
        @NotNull Comparator<Project> comparator = Sort.BY_CREATED.getComparator();
        @NotNull List<Project> projects = projectRepository.findAll(comparator);
        projectList.sort(comparator);
        Assert.assertEquals(projectList, projects);
    }

    @Test
    @SuppressWarnings("unchecked assignment")
    public void testFindAllSortByStatus() {
        @NotNull Comparator<Project> comparator = Sort.BY_STATUS.getComparator();
        @NotNull List<Project> projects = projectRepository.findAll(comparator);
        projectList.sort(comparator);
        Assert.assertEquals(projectList, projects);
    }

    @Test
    public void testFindAllForUser() {
        @NotNull List<Project> projectListForUser = projectList
                .stream()
                .filter(m -> USER_ID_1.equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull List<Project> projects = projectRepository.findAll(USER_ID_1);
        Assert.assertEquals(projectListForUser, projects);
    }

    @Test
    @SuppressWarnings("unchecked assignment")
    public void testFindAllForUserSortByName() {
        @NotNull List<Project> projectListForUser = projectList
                .stream()
                .filter(m -> USER_ID_1.equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull Comparator<Project> comparator = Sort.BY_NAME.getComparator();
        @NotNull List<Project> projects = projectRepository.findAll(USER_ID_1, comparator);
        projectListForUser.sort(comparator);
        Assert.assertEquals(projectListForUser, projects);
    }

    @Test
    @SuppressWarnings("unchecked assignment")
    public void testFindAllForUserSortByCreated() {
        @NotNull List<Project> projectListForUser = projectList
                .stream()
                .filter(m -> USER_ID_1.equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull Comparator<Project> comparator = Sort.BY_CREATED.getComparator();
        @NotNull List<Project> projects = projectRepository.findAll(USER_ID_1, comparator);
        projectListForUser.sort(comparator);
        Assert.assertEquals(projectListForUser, projects);
    }

    @Test
    @SuppressWarnings("unchecked assignment")
    public void testFindAllForUserSortByStatus() {
        @NotNull List<Project> projectListForUser = projectList
                .stream()
                .filter(m -> USER_ID_2.equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull Comparator<Project> comparator = Sort.BY_STATUS.getComparator();
        @NotNull List<Project> projects = projectRepository.findAll(USER_ID_2, comparator);
        projectListForUser.sort(comparator);
        Assert.assertEquals(projectListForUser, projects);
    }

    @Test
    public void testFindOneById() {
        @Nullable Project project;
        for (int i = 0; i < projectList.size(); i++) {
            project = projectRepository.findOneByIndex(i);
            Assert.assertNotNull(project);
            @NotNull final String projectId = project.getId();
            @Nullable final Project foundProject = projectRepository.findOneById(projectId);
            Assert.assertNotNull(foundProject);
        }
    }

    @Test
    public void testFindOneByNullId() {
        @Nullable final Project foundProject = projectRepository.findOneById("meow");
        Assert.assertNull(foundProject);
        Assert.assertThrows(EntityNotFoundException.class, () -> projectRepository.findOneById(""));
        Assert.assertThrows(EntityNotFoundException.class, () -> projectRepository.findOneById(null));
    }

    @Test
    public void testFindOneByIdForUser() {
        @Nullable Project project;
        @NotNull final List<Project> userProjects = projectList
                .stream()
                .filter(m -> USER_ID_1.equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 0; i < userProjects.size(); i++) {
            project = projectRepository.findOneByIndex(USER_ID_1, i);
            Assert.assertNotNull(project);
            @NotNull final String projectId = project.getId();
            @Nullable final Project foundProject = projectRepository.findOneById(USER_ID_1, projectId);
            Assert.assertNotNull(foundProject);
        }
    }

    @Test
    public void testFindOneByNullIdForUser() {
        @Nullable final Project foundProject = projectRepository.findOneById(USER_ID_1, "meow");
        Assert.assertNull(foundProject);
        Assert.assertThrows(EntityNotFoundException.class, () -> projectRepository.findOneById(USER_ID_1, ""));
        Assert.assertThrows(EntityNotFoundException.class, () -> projectRepository.findOneById(USER_ID_1, null));
    }

    @Test
    public void testFindOneByIndex() {
        for (int i = 0; i < projectList.size(); i++) {
            @Nullable final Project project = projectRepository.findOneByIndex(i);
            Assert.assertNotNull(project);
        }
    }

    @Test
    public void testFindOneByNullIndex() {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectRepository.findOneByIndex(projectList.size()));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectRepository.findOneByIndex(-1));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectRepository.findOneByIndex(null));
    }

    @Test
    public void testFindOneByIndexForUser() {
        @NotNull final List<Project> userProjects = projectList
                .stream()
                .filter(m -> USER_ID_1.equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 0; i < userProjects.size(); i++) {
            @Nullable final Project project = projectRepository.findOneByIndex(USER_ID_1, i);
            Assert.assertNotNull(project);
        }
    }

    @Test
    public void testFindOneByNullIndexForUser() {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectRepository.findOneByIndex(USER_ID_1, projectList.size()));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectRepository.findOneByIndex(USER_ID_1, -1));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectRepository.findOneByIndex(USER_ID_1, null));
    }

    @Test
    public void testGetSize() {
        int expectedSize = projectList.size();
        int actualSize = projectRepository.getSize();
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void testGetSizeForUser() {
        int expectedSize = (int) projectList
                .stream()
                .filter(m -> USER_ID_1.equals(m.getUserId()))
                .count();
        int actualSize = projectRepository.getSize(USER_ID_1);
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void testRemove() {
        @Nullable final Project project = projectRepository.findOneByIndex(0);
        Assert.assertNotNull(project);
        @NotNull final String projectId = project.getId();
        @Nullable final Project deletedProject = projectRepository.remove(project);
        Assert.assertNotNull(deletedProject);
        Assert.assertFalse(projectRepository.existsById(projectId));
    }

    @Test
    public void testRemoveNull() {
        Assert.assertThrows(EntityNotFoundException.class, () -> projectRepository.remove(null));
    }

    @Test
    public void testRemoveAllNull() {
        int expectedCount = projectRepository.getSize();
        projectRepository.removeAll(null);
        int actualCount = projectRepository.getSize();
        Assert.assertEquals(expectedCount, actualCount);
    }

    @Test
    public void testRemoveById() {
        int index = projectList.size();
        while (index > 0) {
            @Nullable final Project project = projectRepository.findOneByIndex(index - 1);
            Assert.assertNotNull(project);
            @NotNull final String projectId = project.getId();
            @Nullable final Project deletedProject = projectRepository.removeById(projectId);
            Assert.assertNotNull(deletedProject);
            Assert.assertFalse(projectRepository.existsById(projectId));
            index--;
        }
    }

    @Test
    public void testRemoveByNullId() {
        Assert.assertThrows(EntityNotFoundException.class, () -> projectRepository.removeById("meow"));
        Assert.assertThrows(EntityNotFoundException.class, () -> projectRepository.removeById(""));
        Assert.assertThrows(EntityNotFoundException.class, () -> projectRepository.removeById(null));
    }

    @Test
    public void testRemoveByIdForUser() {
        int index = (int) projectList
                .stream()
                .filter(m -> USER_ID_1.equals(m.getUserId()))
                .count();
        while (index > 0) {
            @Nullable final Project project = projectRepository.findOneByIndex(USER_ID_1, index - 1);
            Assert.assertNotNull(project);
            @NotNull final String projectId = project.getId();
            @Nullable final Project deletedProject = projectRepository.removeById(USER_ID_1, projectId);
            Assert.assertNotNull(deletedProject);
            Assert.assertFalse(projectRepository.existsById(USER_ID_1, projectId));
            index--;
        }
    }

    @Test
    public void testRemoveByNullIdForUser() {
        Assert.assertThrows(EntityNotFoundException.class, () -> projectRepository.removeById(USER_ID_1, "meow"));
        Assert.assertThrows(EntityNotFoundException.class, () -> projectRepository.removeById(USER_ID_1, ""));
        Assert.assertThrows(EntityNotFoundException.class, () -> projectRepository.removeById(USER_ID_1, null));
    }

    @Test
    public void testRemoveByIndex() {
        int index = projectList.size();
        while (index > 0) {
            @Nullable final Project deletedProject = projectRepository.removeByIndex(index - 1);
            Assert.assertNotNull(deletedProject);
            @NotNull final String projectId = deletedProject.getId();
            Assert.assertFalse(projectRepository.existsById(projectId));
            index--;
        }
    }

    @Test
    public void testRemoveByNullIndex() {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectRepository.removeByIndex(-1));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectRepository.removeByIndex(projectRepository.getSize()));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectRepository.removeByIndex(null));
    }

    @Test
    public void testRemoveByIndexForUser() {
        int index = (int) projectList
                .stream()
                .filter(m -> USER_ID_1.equals(m.getUserId()))
                .count();
        while (index > 0) {
            @Nullable final Project deletedProject = projectRepository.removeByIndex(USER_ID_1, index - 1);
            Assert.assertNotNull(deletedProject);
            @NotNull final String projectId = deletedProject.getId();
            Assert.assertFalse(projectRepository.existsById(USER_ID_1, projectId));
            index--;
        }
    }

    @Test
    public void testRemoveByNullIndexForUser() {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectRepository.removeByIndex(USER_ID_1, -1));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectRepository.removeByIndex(USER_ID_1, projectRepository.getSize()));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectRepository.removeByIndex(USER_ID_1, null));
    }

    @Test
    public void setTest() {
        int expectedNumberOfEntries = 2;
        @NotNull final List<Project> projects = new ArrayList<>();
        @NotNull final String firstProjectName = "First Project Name";
        @NotNull final String firstProjectDescription = "Project Description";
        @NotNull final Project firstProject = new Project();
        firstProject.setName(firstProjectName);
        firstProject.setDescription(firstProjectDescription);
        projects.add(firstProject);
        @NotNull final String secondProjectName = "Second Project Name";
        @NotNull final String secondProjectDescription = "Project Description";
        @NotNull final Project secondProject = new Project();
        secondProject.setName(secondProjectName);
        secondProject.setDescription(secondProjectDescription);
        projects.add(secondProject);
        @NotNull final Collection<Project> addedProjects = projectRepository.set(projects);
        Assert.assertTrue(addedProjects.size() > 0);
        int actualNumberOfEntries = projectRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

}