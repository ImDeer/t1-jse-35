package t1.dkhrunina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.request.task.TaskListRequest;
import t1.dkhrunina.tm.enumerated.Sort;
import t1.dkhrunina.tm.model.Task;
import t1.dkhrunina.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "t-list";

    @NotNull
    private static final String DESCRIPTION = "Show task list.";

    @Override
    public void execute() {
        System.out.println("[Task list]");
        System.out.println("Enter sort type (optional): ");
        System.out.println(Arrays.toString(Sort.toValues()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);
        @NotNull final TaskListRequest request = new TaskListRequest(getToken(), sort);
        @Nullable final List<Task> tasks = getTaskEndpoint().listTask(request).getTasks();
        renderTasks(tasks);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}