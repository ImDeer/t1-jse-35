package t1.dkhrunina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.request.project.ProjectFindOneByIdRequest;
import t1.dkhrunina.tm.model.Project;
import t1.dkhrunina.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @NotNull
    private static final String NAME = "pr-show-by-id";

    @NotNull
    private static final String DESCRIPTION = "Show project by id.";

    @Override
    public void execute() {
        System.out.println("[Show project by id]");
        System.out.println("Enter id: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectFindOneByIdRequest request = new ProjectFindOneByIdRequest(getToken(), id);
        @Nullable final Project project = getProjectEndpoint().findOneById(request).getProject();
        showProject(project);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}