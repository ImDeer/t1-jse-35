package t1.dkhrunina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import t1.dkhrunina.tm.api.repository.IProjectRepository;
import t1.dkhrunina.tm.api.repository.ITaskRepository;
import t1.dkhrunina.tm.api.repository.IUserRepository;
import t1.dkhrunina.tm.api.service.*;
import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.enumerated.Sort;
import t1.dkhrunina.tm.enumerated.Status;
import t1.dkhrunina.tm.exception.entity.EntityNotFoundException;
import t1.dkhrunina.tm.exception.entity.TaskNotFoundException;
import t1.dkhrunina.tm.exception.field.*;
import t1.dkhrunina.tm.model.Project;
import t1.dkhrunina.tm.model.Task;
import t1.dkhrunina.tm.model.User;
import t1.dkhrunina.tm.repository.ProjectRepository;
import t1.dkhrunina.tm.repository.TaskRepository;
import t1.dkhrunina.tm.repository.UserRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TaskServiceTest {

    @NotNull
    private List<Task> taskList;

    @NotNull
    private IProjectService projectService;

    @NotNull
    private ITaskService taskService;

    @NotNull
    private User user;

    @NotNull
    private User admin;

    @Before
    public void initService() {
        @NotNull final IProjectRepository projectRepository = new ProjectRepository();
        @NotNull final IUserRepository userRepository = new UserRepository();
        @NotNull final ITaskRepository taskRepository = new TaskRepository();
        @NotNull final IPropertyService propertyService = new PropertyService();
        projectService = new ProjectService(projectRepository);
        taskService = new TaskService(taskRepository);
        @NotNull final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
        @NotNull final IUserService userService = new UserService(propertyService, userRepository, projectTaskService);
        taskList = new ArrayList<>();
        user = userService.create("USER", "USER", "user@user.user");
        admin = userService.create("ADMIN", "ADMIN", Role.ADMIN);
        @NotNull final Project project1 = projectService.create(user.getId(), "project 1", "user project 1");
        @NotNull final Project project2 = projectService.create(user.getId(), "project 2", "user project 2");
        @NotNull final Project project3 = projectService.create(admin.getId(), "project 3", "admin project");
        @NotNull final Task task1 = taskService.create(user.getId(), "task 1", "project 1 task");
        task1.setProjectId(project1.getId());
        @NotNull final Task task2 = taskService.create(user.getId(), "task 2", "project 2 task");
        task2.setProjectId(project2.getId());
        @NotNull final Task task3 = taskService.create(user.getId(), "task 3", "project 1 task");
        task3.setProjectId(project1.getId());
        @NotNull final Task task4 = taskService.create(admin.getId(), "task 4", "project 3 task");
        task4.setProjectId(project3.getId());
        @NotNull final Task task5 = taskService.create(admin.getId(), "task 5", "user task");
        taskList.add(task1);
        taskList.add(task2);
        taskList.add(task3);
        taskList.add(task4);
        taskList.add(task5);
    }

    @Test
    public void testAdd() {
        int expectedNumberOfEntries = taskService.getSize() + 1;
        @NotNull final Task task = new Task();
        task.setUserId("meow");
        task.setName("Test Name");
        task.setDescription("Test Description");
        taskService.add(task);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    public void testAddForUser() {
        int expectedNumberOfEntries = taskService.getSize(user.getId()) + 1;
        @NotNull final Task task = new Task();
        task.setUserId(user.getId());
        task.setName("Test Name");
        task.setDescription("Test Description");
        taskService.add(user.getId(), task);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize(user.getId()));
    }

    @Test
    public void testAddNullForUser() {
        int expectedNumberOfEntries = taskService.getSize(user.getId());
        @Nullable final Task task = taskService.add(user.getId(), null);
        Assert.assertNull(task);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize(user.getId()));
    }

    @Test
    public void testAddCollection() {
        int expectedNumberOfEntries = taskService.getSize() + 2;
        @NotNull final List<Task> taskList = new ArrayList<>();
        @NotNull final Task firstTask = new Task();
        firstTask.setUserId("meow");
        firstTask.setName("Test Name 1");
        firstTask.setDescription("Test Description 1");
        taskList.add(firstTask);
        @NotNull final Task secondTask = new Task();
        secondTask.setUserId("meow");
        secondTask.setName("Test Name 2");
        secondTask.setDescription("Test Description 2");
        taskList.add(secondTask);
        taskService.add(taskList);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    public void testChangeStatusById() {
        @NotNull final List<Task> tasks = taskService.findAll(user.getId());
        for (@NotNull final Task task : tasks) {
            @NotNull final String taskId = task.getId();
            @Nullable Task changedTask = taskService.changeTaskStatusById(user.getId(), taskId, Status.IN_PROGRESS);
            Assert.assertNotNull(changedTask);
            changedTask = taskService.findOneById(taskId);
            Assert.assertNotNull(changedTask);
            Assert.assertEquals(Status.IN_PROGRESS, changedTask.getStatus());
        }
    }

    @Test
    public void testChangeStatusByIdEmpty() {
        Assert.assertThrows(TaskIdEmptyException.class, () -> taskService.changeTaskStatusById(user.getId(), "", Status.IN_PROGRESS));
    }

    @Test
    public void testChangeStatusByIdNull() {
        Assert.assertThrows(TaskIdEmptyException.class, () -> taskService.changeTaskStatusById(user.getId(), null, Status.IN_PROGRESS));
    }

    @Test
    public void testChangeStatusByIdNotFound() {
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.changeTaskStatusById(user.getId(), "123", Status.IN_PROGRESS));
    }

    @Test
    public void testChangeStatusIncorrectById() {
        Assert.assertThrows(StatusIncorrectException.class, () -> taskService.changeTaskStatusById(user.getId(), "meow", null));
    }

    @Test
    public void testChangeStatusByIndex() {
        @NotNull final List<Task> tasks = taskService.findAll(user.getId());
        for (int i = 0; i < tasks.size(); i++) {
            @NotNull final Task task = tasks.get(i);
            @NotNull final String taskId = task.getId();
            @Nullable Task changedTask = taskService.changeTaskStatusByIndex(user.getId(), i, Status.IN_PROGRESS);
            Assert.assertNotNull(changedTask);
            changedTask = taskService.findOneById(taskId);
            Assert.assertNotNull(changedTask);
            Assert.assertEquals(Status.IN_PROGRESS, changedTask.getStatus());
        }
    }

    @Test
    public void testChangeStatusByIndexNull() {
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.changeTaskStatusByIndex(user.getId(), null, Status.IN_PROGRESS));
    }

    @Test
    public void testChangeStatusByIndexNegative() {
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.changeTaskStatusByIndex(user.getId(), -1, Status.IN_PROGRESS));
    }

    @Test
    public void testChangeStatusByIndexInvalid() {
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.changeTaskStatusByIndex(user.getId(), 100, Status.IN_PROGRESS));
    }

    @Test
    public void testChangeStatusIncorrectByIndex() {
        Assert.assertThrows(StatusIncorrectException.class, () -> taskService.changeTaskStatusByIndex(user.getId(), 0, null));
    }

    @Test
    public void testClear() {
        int expectedNumberOfEntries = 0;
        Assert.assertTrue(taskService.getSize() > 0);
        taskService.clear();
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    public void testClearForUser() {
        int expectedNumberOfEntries = 0;
        Assert.assertTrue(taskService.getSize(user.getId()) > 0);
        taskService.clear(user.getId());
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize(user.getId()));
    }

    @Test
    public void testCreate() {
        int expectedNumberOfEntries = taskService.getSize(user.getId()) + 1;
        @NotNull final String name = "Task name";
        @NotNull final String description = "Task Description";
        @Nullable Task createdTask = taskService.create(user.getId(), name, description);
        @NotNull final String taskId = createdTask.getId();
        Assert.assertNotNull(createdTask);
        createdTask = taskService.findOneById(user.getId(), taskId);
        Assert.assertNotNull(createdTask);
        Assert.assertEquals(name, createdTask.getName());
        Assert.assertEquals(description, createdTask.getDescription());
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize(user.getId()));
    }

    @Test
    public void testCreateNameEmpty() {
        Assert.assertThrows(NameEmptyException.class, () -> taskService.create(user.getId(), "", "description"));
    }

    @Test
    public void testCreateNameNull() {
        Assert.assertThrows(NameEmptyException.class, () -> taskService.create(user.getId(), null, "description"));
    }

    @Test
    public void testCreateDescriptionEmpty() {
        Assert.assertThrows(DescriptionEmptyException.class, () -> taskService.create(user.getId(), "name", ""));
    }

    @Test
    public void testCreateDescriptionNull() {
        Assert.assertThrows(DescriptionEmptyException.class, () -> taskService.create(user.getId(), "name", null));
    }

    @Test
    public void testCreateByName() {
        int expectedNumberOfEntries = taskService.getSize(user.getId()) + 1;
        @NotNull final String name = "Task name";
        @Nullable Task createdTask = taskService.create(user.getId(), name);
        @NotNull final String taskId = createdTask.getId();
        Assert.assertNotNull(createdTask);
        createdTask = taskService.findOneById(user.getId(), taskId);
        Assert.assertNotNull(createdTask);
        Assert.assertEquals(name, createdTask.getName());
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize(user.getId()));
    }

    @Test
    public void testCreateByNameEmpty() {
        Assert.assertThrows(NameEmptyException.class, () -> taskService.create(user.getId(), ""));
    }

    @Test
    public void testCreateByNameNull() {
        Assert.assertThrows(NameEmptyException.class, () -> taskService.create(user.getId(), null));
    }

    @Test
    public void testSet() {
        int expectedNumberOfEntries = 2;
        @NotNull final List<Task> taskList = new ArrayList<>();
        @NotNull final Task firstTask = new Task();
        firstTask.setUserId("meow");
        firstTask.setName("Test Name");
        firstTask.setDescription("Test Description");
        taskList.add(firstTask);
        @NotNull final Task secondTask = new Task();
        secondTask.setUserId("meow");
        secondTask.setName("Test Name");
        secondTask.setDescription("Test Description");
        taskList.add(secondTask);
        taskService.set(taskList);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    public void testExistById() {
        for (@NotNull final Task task : taskList) {
            final boolean isExist = taskService.existsById(task.getId());
            Assert.assertTrue(isExist);
        }
    }

    @Test
    public void testExistByIdFalse() {
        final boolean isExist = taskService.existsById("123321");
        Assert.assertFalse(isExist);
    }

    @Test
    public void testExistByIdForUser() {
        @NotNull final List<Task> tasksForTestUser = taskList.stream().filter(m -> user.getId().equals(m.getUserId())).collect(Collectors.toList());
        for (@NotNull final Task task : tasksForTestUser) {
            final boolean isExist = taskService.existsById(user.getId(), task.getId());
            Assert.assertTrue(isExist);
        }
    }

    @Test
    public void testExistByIdForUserFalse() {
        final boolean isExist = taskService.existsById("meow", "123321");
        Assert.assertFalse(isExist);
    }

    @Test
    public void testFindAllByProjectId() {
        @Nullable final Project project = projectService.findOneByIndex(user.getId(), 0);
        Assert.assertNotNull(project);
        @NotNull final String projectId = project.getId();
        @NotNull final List<Task> tasksToFind = taskList.stream().filter(m -> user.getId().equals(m.getUserId())).filter(m -> projectId.equals(m.getProjectId())).collect(Collectors.toList());
        @NotNull final List<Task> tasksByProjectId = taskService.findAllByProjectId(user.getId(), projectId);
        Assert.assertEquals(tasksToFind, tasksByProjectId);
    }

    @Test
    public void testFindAllByProjectIdEmpty() {
        @NotNull final List<Task> emptyList = Collections.emptyList();
        @NotNull final List<Task> tasksByProjectId = taskService.findAllByProjectId(user.getId(), "");
        Assert.assertEquals(emptyList, tasksByProjectId);
    }

    @Test
    public void testFindAllByProjectIdNull() {
        @NotNull final List<Task> emptyList = Collections.emptyList();
        @NotNull final List<Task> tasksByProjectId = taskService.findAllByProjectId(user.getId(), null);
        Assert.assertEquals(emptyList, tasksByProjectId);
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Task> tasks = taskService.findAll();
        Assert.assertEquals(taskList, tasks);
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<Task> tasksForTestUser = taskList.stream().filter(m -> user.getId().equals(m.getUserId())).collect(Collectors.toList());
        @NotNull final List<Task> tasks = taskService.findAll(user.getId());
        Assert.assertEquals(tasksForTestUser, tasks);
    }

    @Test
    public void testFindAllWithComparator() {
        @Nullable Comparator<Task> comparator = Sort.BY_NAME.getComparator();
        @NotNull List<Task> tasks = taskService.findAll(comparator);
        Assert.assertEquals(taskList, tasks);
        comparator = Sort.BY_CREATED.getComparator();
        tasks = taskService.findAll(comparator);
        Assert.assertEquals(taskList, tasks);
        comparator = Sort.BY_STATUS.getComparator();
        tasks = taskService.findAll(comparator);
        Assert.assertEquals(taskList, tasks);
        comparator = null;
        tasks = taskService.findAll(comparator);
        Assert.assertEquals(taskList, tasks);
    }

    @Test
    public void testFindAllWithComparatorForUser() {
        @NotNull final List<Task> tasksForTestUser = taskList.stream().filter(m -> user.getId().equals(m.getUserId())).collect(Collectors.toList());
        @Nullable Comparator<Task> comparator = Sort.BY_NAME.getComparator();
        @NotNull List<Task> tasks = taskService.findAll(user.getId(), comparator);
        Assert.assertEquals(tasksForTestUser, tasks);
        comparator = Sort.BY_CREATED.getComparator();
        tasks = taskService.findAll(user.getId(), comparator);
        Assert.assertEquals(tasksForTestUser, tasks);
        comparator = Sort.BY_STATUS.getComparator();
        tasks = taskService.findAll(user.getId(), comparator);
        Assert.assertEquals(tasksForTestUser, tasks);
    }

    @Test
    public void testFindAllWithSortForUser() {
        @NotNull final List<Task> tasksForTestUser = taskList.stream().filter(m -> user.getId().equals(m.getUserId())).collect(Collectors.toList());
        @NotNull List<Task> tasks = taskService.findAll(user.getId(), Sort.BY_NAME);
        Assert.assertEquals(tasksForTestUser, tasks);
        tasks = taskService.findAll(user.getId(), Sort.BY_CREATED);
        Assert.assertEquals(tasksForTestUser, tasks);
        tasks = taskService.findAll(user.getId(), Sort.BY_STATUS);
        Assert.assertEquals(tasksForTestUser, tasks);
        @Nullable final Sort sort = null;
        tasks = taskService.findAll(user.getId(), sort);
        Assert.assertEquals(tasksForTestUser, tasks);
    }

    @Test
    public void testFindOneById() {
        @Nullable Task foundTask;
        for (@NotNull final Task task : taskList) {
            foundTask = taskService.findOneById(task.getId());
            Assert.assertNotNull(foundTask);
        }
    }

    @Test
    public void testFindOneByIdNull() {
        @Nullable final Task foundTask = taskService.findOneById(null);
        Assert.assertNull(foundTask);
    }

    @Test
    public void testFindOneByIdEmpty() {
        @Nullable final Task foundTask = taskService.findOneById("");
        Assert.assertNull(foundTask);
    }

    @Test
    public void testFindOneByIdForUser() {
        @Nullable Task foundTask;
        @NotNull final List<Task> tasksForTestUser = taskList.stream().filter(m -> user.getId().equals(m.getUserId())).collect(Collectors.toList());
        for (@NotNull final Task task : tasksForTestUser) {
            foundTask = taskService.findOneById(user.getId(), task.getId());
            Assert.assertNotNull(foundTask);
        }
    }

    @Test
    public void testFindOneByIdNullForUser() {
        @Nullable final Task foundTask = taskService.findOneById(user.getId(), null);
        Assert.assertNull(foundTask);
    }

    @Test
    public void testFindOneByIdEmptyForUser() {
        @Nullable final Task foundTask = taskService.findOneById(user.getId(), "");
        Assert.assertNull(foundTask);
    }

    @Test
    public void testFindOneByIndex() {
        for (int i = 0; i < taskList.size(); i++) {
            @Nullable final Task task = taskService.findOneByIndex(i);
            Assert.assertNotNull(task);
        }
    }

    @Test
    public void testFindOneByIndexInvalid() {
        int index = taskService.getSize() + 1;
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.findOneByIndex(index));
    }

    @Test
    public void testFindOneByIndexNull() {
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.findOneByIndex(null));
    }

    @Test
    public void testFindOneByIndexNegative() {
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.findOneByIndex(-1));
    }

    @Test
    public void testFindOneByIndexForUser() {
        @NotNull final List<Task> tasksForTestUser = taskList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 0; i < tasksForTestUser.size(); i++) {
            @Nullable final Task task = taskService.findOneByIndex(user.getId(), i);
            Assert.assertNotNull(task);
        }
    }

    @Test
    public void testFindOneByIndexInvalidForUser() {
        int index = taskService.getSize(user.getId()) + 1;
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.findOneByIndex(user.getId(), index));
    }

    @Test
    public void testFindOneByIndexNullForUser() {
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.findOneByIndex(user.getId(), null));
    }

    @Test
    public void testFindOneByIndexNegativeForUser() {
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.findOneByIndex(user.getId(), -1));
    }

    @Test
    public void testGetSize() {
        int expectedSize = taskList.size();
        int actualSize = taskService.getSize();
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void testGetSizeForUser() {
        int expectedSize = (int) taskList.stream().filter(m -> user.getId().equals(m.getUserId())).count();
        int actualSize = taskService.getSize(user.getId());
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void testRemove() {
        for (@NotNull final Task task : taskList) {
            @NotNull final String taskId = task.getId();
            @Nullable final Task deletedTask = taskService.remove(task);
            Assert.assertNotNull(deletedTask);
            @Nullable final Task deletedTaskInRepository = taskService.findOneById(taskId);
            Assert.assertNull(deletedTaskInRepository);
        }
    }

    @Test
    public void testRemoveEntityNotFound() {
        @NotNull final Task task = new Task();
        Assert.assertThrows(EntityNotFoundException.class, () -> taskService.remove(task));
    }

    @Test
    public void testRemoveEntityNull() {
        Assert.assertThrows(EntityNotFoundException.class, () -> taskService.remove(null));
    }

    @Test
    public void testRemoveAll() {
        int expectedNumberOfEntries = 0;
        @NotNull final List<Task> tasksForTestUser = taskList.stream().filter(m -> user.getId().equals(m.getUserId())).collect(Collectors.toList());
        taskService.removeAll(tasksForTestUser);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize(user.getId()));
    }

    @Test
    public void testRemoveById() {
        for (@NotNull final Task task : taskList) {
            @NotNull final String taskId = task.getId();
            @Nullable final Task deletedTask = taskService.removeById(taskId);
            Assert.assertNotNull(deletedTask);
            @Nullable final Task deletedTaskInRepository = taskService.findOneById(taskId);
            Assert.assertNull(deletedTaskInRepository);
        }
    }

    @Test
    public void testRemoveByIdNull() {
        Assert.assertThrows(IdEmptyException.class, () -> taskService.removeById(null));
    }

    @Test
    public void testRemoveByIdEmpty() {
        Assert.assertThrows(IdEmptyException.class, () -> taskService.removeById(""));
    }

    @Test
    public void testRemoveByIdNotFound() {
        Assert.assertThrows(EntityNotFoundException.class, () -> taskService.removeById("123321"));
    }

    @Test
    public void testRemoveByIdForUser() {
        @NotNull final List<Task> tasksForTestUser = taskList.stream().filter(m -> user.getId().equals(m.getUserId())).collect(Collectors.toList());
        for (@NotNull final Task task : tasksForTestUser) {
            @NotNull final String taskId = task.getId();
            @Nullable final Task deletedTask = taskService.removeById(user.getId(), taskId);
            Assert.assertNotNull(deletedTask);
            @Nullable final Task deletedTaskInRepository = taskService.findOneById(user.getId(), taskId);
            Assert.assertNull(deletedTaskInRepository);
        }
    }

    @Test
    public void testRemoveByIdNullForUser() {
        Assert.assertThrows(IdEmptyException.class, () -> taskService.removeById(user.getId(), null));
    }

    @Test
    public void testRemoveByIdEmptyForUser() {
        Assert.assertThrows(IdEmptyException.class, () -> taskService.removeById(user.getId(), ""));
    }

    @Test
    public void testRemoveByIdNotFoundForUser() {
        Assert.assertThrows(EntityNotFoundException.class, () -> taskService.removeById(user.getId(), "123321"));
    }

    @Test
    public void testRemoveByIndex() {
        int index = taskList.size();
        while (index > 0) {
            @Nullable final Task deletedTask = taskService.removeByIndex(index - 1);
            Assert.assertNotNull(deletedTask);
            @NotNull final String taskId = deletedTask.getId();
            @Nullable final Task deletedTaskInRepository = taskService.findOneById(taskId);
            Assert.assertNull(deletedTaskInRepository);
            index--;
        }
    }

    @Test
    public void testRemoveByIndexInvalid() {
        int index = taskList.size() + 1;
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.removeByIndex(index));
    }

    @Test
    public void testRemoveByIndexNull() {
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.removeByIndex(null));
    }

    @Test
    public void testRemoveByIndexNegative() {
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.removeByIndex(-1));
    }

    @Test
    public void testRemoveByIndexForUser() {
        int index = (int) taskList.stream().filter(m -> user.getId().equals(m.getUserId())).count();
        while (index > 0) {
            @Nullable final Task deletedTask = taskService.removeByIndex(user.getId(), index - 1);
            Assert.assertNotNull(deletedTask);
            @NotNull final String taskId = deletedTask.getId();
            @Nullable final Task deletedTaskInRepository = taskService.findOneById(user.getId(), taskId);
            Assert.assertNull(deletedTaskInRepository);
            index--;
        }
    }

    @Test
    public void testRemoveByIndexInvalidForUser() {
        int index = taskList.size() + 1;
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.removeByIndex(user.getId(), index));
    }

    @Test
    public void testRemoveByIndexNullForUser() {
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.removeByIndex(user.getId(), null));
    }

    @Test
    public void testRemoveByIndexNegativeForUser() {
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.removeByIndex(user.getId(), -1));
    }

    @Test
    public void testUpdateById() {
        @NotNull final List<Task> tasks = taskService.findAll(user.getId());
        @NotNull String name;
        @NotNull String description;
        int index = 0;
        for (@NotNull final Task task : tasks) {
            @NotNull final String taskId = task.getId();
            name = "name " + index;
            description = "description" + index;
            @Nullable Task updatedTask = taskService.updateById(user.getId(), taskId, name, description);
            Assert.assertNotNull(updatedTask);
            updatedTask = taskService.findOneById(taskId);
            Assert.assertNotNull(updatedTask);
            Assert.assertEquals(name, updatedTask.getName());
            Assert.assertEquals(description, updatedTask.getDescription());
            index++;
        }
    }

    @Test
    public void testUpdateByIdEmpty() {
        Assert.assertThrows(IdEmptyException.class, () -> taskService.updateById(user.getId(), "", "name", "description"));
    }

    @Test
    public void testUpdateByIdNull() {
        Assert.assertThrows(IdEmptyException.class, () -> taskService.updateById(user.getId(), null, "name", "description"));
    }

    @Test
    public void testUpdateByIdNotFound() {
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.updateById(user.getId(), "meow", "name", "description"));
    }

    @Test
    public void testUpdateByIdNameEmpty() {
        @NotNull final String id = taskService.findOneByIndex(user.getId(), 0).getId();
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateById(user.getId(), id, "", "description"));
    }

    @Test
    public void testUpdateByIdNameNull() {
        @NotNull final String id = taskService.findOneByIndex(user.getId(), 0).getId();
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateById(user.getId(), id, null, "description"));
    }

    @Test
    public void testUpdateByIndex() {
        @NotNull final List<Task> tasks = taskService.findAll(user.getId());
        @NotNull String name;
        @NotNull String description;
        int index = 0;
        for (@NotNull final Task task : tasks) {
            @NotNull final String taskId = task.getId();
            name = "name " + index;
            description = "description" + index;
            @Nullable Task updatedTask = taskService.updateByIndex(user.getId(), index, name, description);
            Assert.assertNotNull(updatedTask);
            updatedTask = taskService.findOneById(taskId);
            Assert.assertNotNull(updatedTask);
            Assert.assertEquals(name, updatedTask.getName());
            Assert.assertEquals(description, updatedTask.getDescription());
            index++;
        }
    }

    @Test
    public void testUpdateByIndexNull() {
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.updateByIndex(user.getId(), null, "name", "description"));
    }

    @Test
    public void testUpdateByIndexNegative() {
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.updateByIndex(user.getId(), -1, "name", "description"));
    }

    @Test
    public void testUpdateByIndexInvalid() {
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.updateByIndex(user.getId(), 100, "name", "description"));
    }

    @Test
    public void testUpdateByIndexNameNull() {
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateByIndex(user.getId(), 0, null, "description"));
    }

    @Test
    public void testUpdateByIndexNameEmpty() {
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateByIndex(user.getId(), 0, "", "description"));
    }

}