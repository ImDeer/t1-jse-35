package t1.dkhrunina.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.api.endpoint.IAuthEndpoint;
import t1.dkhrunina.tm.api.service.IAuthService;
import t1.dkhrunina.tm.api.service.IServiceLocator;
import t1.dkhrunina.tm.dto.request.user.UserLoginRequest;
import t1.dkhrunina.tm.dto.request.user.UserLogoutRequest;
import t1.dkhrunina.tm.dto.request.user.UserShowProfileRequest;
import t1.dkhrunina.tm.dto.response.user.UserLoginResponse;
import t1.dkhrunina.tm.dto.response.user.UserLogoutResponse;
import t1.dkhrunina.tm.dto.response.user.UserShowProfileResponse;
import t1.dkhrunina.tm.model.Session;
import t1.dkhrunina.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "t1.dkhrunina.tm.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLoginResponse loginUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLoginRequest request
    ) {
        @NotNull final IAuthService authService = getServiceLocator().getAuthService();
        @NotNull final String token = authService.login(request.getLogin(), request.getPassword());
        return new UserLoginResponse(token);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLogoutResponse logoutUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLogoutRequest request
    ) {
        @NotNull final IAuthService authService = getServiceLocator().getAuthService();
        authService.logout(request.getToken());
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserShowProfileResponse showUserProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserShowProfileRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final User user = getServiceLocator().getUserService().findOneById(userId);
        return new UserShowProfileResponse(user);
    }

}