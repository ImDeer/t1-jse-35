package t1.dkhrunina.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.dto.response.AbstractResultResponse;
import t1.dkhrunina.tm.model.Task;

@Getter
@Setter
@NoArgsConstructor
public class TaskFindOneByIdResponse extends AbstractResultResponse {

    @Nullable
    private Task task;

    public TaskFindOneByIdResponse(@Nullable final Task task) {
        this.task = task;
    }

}