package t1.dkhrunina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import t1.dkhrunina.tm.api.repository.IUserRepository;
import t1.dkhrunina.tm.exception.entity.EntityNotFoundException;
import t1.dkhrunina.tm.exception.field.IndexIncorrectException;
import t1.dkhrunina.tm.model.User;
import t1.dkhrunina.tm.service.PropertyService;
import t1.dkhrunina.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class UserRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private List<User> userList;

    @NotNull
    private IUserRepository userRepository;

    @Before
    public void initRepository() {
        userList = new ArrayList<>();
        userRepository = new UserRepository();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final User user = new User();
            user.setLogin("User " + i);
            user.setEmail("User" + i + "@user.com");
            user.setFirstName("User1" + i);
            user.setLastName("LastName" + i);
            user.setPasswordHash(HashUtil.salt(new PropertyService(), "user" + i));
            userRepository.add(user);
            userList.add(user);
        }
    }

    @Test
    public void testAdd() {
        int expectedNumberOfEntries = userRepository.getSize() + 1;
        @NotNull final String login = "Test";
        @NotNull final String email = "Test@test.com";
        @NotNull final String firstName = "FirstName";
        @NotNull final String lastName = "LastName";
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setEmail(email);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        userRepository.add(user);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
        @Nullable final User createdUser = userRepository.findOneByIndex(expectedNumberOfEntries - 1);
        Assert.assertNotNull(createdUser);
        Assert.assertEquals(login, createdUser.getLogin());
        Assert.assertEquals(email, createdUser.getEmail());
        Assert.assertEquals(firstName, createdUser.getFirstName());
        Assert.assertEquals(lastName, createdUser.getLastName());
    }

    @Test
    public void testAddMultiple() {
        int expectedNumberOfEntries = userRepository.getSize() + 2;
        @NotNull final List<User> users = new ArrayList<>();
        @NotNull final String firstLogin = "Test1";
        @NotNull final String firstEmail = "Test1@test.com";
        @NotNull final String firstFirstName = "FirstName1";
        @NotNull final String firstLastName = "LastName1";
        @NotNull final User firstUser = new User();
        firstUser.setLogin(firstLogin);
        firstUser.setEmail(firstEmail);
        firstUser.setFirstName(firstFirstName);
        firstUser.setLastName(firstLastName);
        users.add(firstUser);
        @NotNull final String secondLogin = "Test2";
        @NotNull final String secondEmail = "Test2@test.com";
        @NotNull final String secondFirstName = "FirstName2";
        @NotNull final String secondLastName = "LastName2";
        @NotNull final User secondUser = new User();
        firstUser.setLogin(secondLogin);
        firstUser.setEmail(secondEmail);
        firstUser.setFirstName(secondFirstName);
        firstUser.setLastName(secondLastName);
        users.add(secondUser);
        userRepository.add(users);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testClear() {
        int expectedNumberOfEntries = 0;
        userRepository.clear();
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testExistByIdTrue() {
        @Nullable final User user = userRepository.findOneByIndex(1);
        Assert.assertNotNull(user);
        @Nullable final String userId = user.getId();
        Assert.assertTrue(userRepository.existsById(userId));
    }

    @Test
    public void testExistByIdFalse() {
        Assert.assertFalse(userRepository.existsById("123123"));
    }

    @Test
    public void testFindAll() {
        @NotNull final List<User> users = userRepository.findAll();
        Assert.assertEquals(userList, users);
    }

    @Test
    public void testFindOneById() {
        @Nullable User user;
        for (int i = 0; i < userList.size(); i++) {
            user = userRepository.findOneByIndex(i);
            Assert.assertNotNull(user);
            @NotNull final String userId = user.getId();
            @Nullable final User foundUser = userRepository.findOneById(userId);
            Assert.assertNotNull(foundUser);
        }
    }

    @Test
    public void testFindOneByNullId() {
        @Nullable final User foundUser = userRepository.findOneById("meow");
        Assert.assertNull(foundUser);
        Assert.assertThrows(EntityNotFoundException.class, () -> userRepository.findOneById(""));
        Assert.assertThrows(EntityNotFoundException.class, () -> userRepository.findOneById(null));
    }

    @Test
    public void testFindOneByIndex() {
        for (int i = 0; i < userList.size(); i++) {
            @Nullable final User user = userRepository.findOneByIndex(i);
            Assert.assertNotNull(user);
        }
    }

    @Test
    public void testFindOneByNullIndex() {
        Assert.assertThrows(IndexIncorrectException.class, () -> userRepository.findOneByIndex(userList.size()));
        Assert.assertThrows(IndexIncorrectException.class, () -> userRepository.findOneByIndex(-1));
        Assert.assertThrows(IndexIncorrectException.class, () -> userRepository.findOneByIndex(null));
    }

    @Test
    public void testFindByLogin() {
        for (@NotNull final User user : userList) {
            @Nullable final String login = user.getLogin();
            @Nullable final User foundUser = userRepository.findByLogin(login);
            Assert.assertEquals(user, foundUser);
        }
    }

    @Test
    public void testFindByNullLogin() {
        Assert.assertNull(userRepository.findByLogin("meow"));
        Assert.assertNull(userRepository.findByLogin(""));
        Assert.assertNull(userRepository.findByLogin(null));
    }

    @Test
    public void testFindByEmail() {
        for (@NotNull final User user : userList) {
            @Nullable final String email = user.getEmail();
            @Nullable final User foundUser = userRepository.findByEmail(email);
            Assert.assertEquals(user, foundUser);
        }
    }

    @Test
    public void testFindByNullEmail() {
        Assert.assertNull(userRepository.findByEmail("meow"));
        Assert.assertNull(userRepository.findByEmail(""));
        Assert.assertNull(userRepository.findByEmail(null));
    }

    @Test
    public void testGetSize() {
        int expectedSize = userList.size();
        int actualSize = userRepository.getSize();
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void testIsEmailExist() {
        for (@NotNull final User user : userList) {
            @Nullable final String email = user.getEmail();
            Assert.assertTrue(userRepository.isEmailExist(email));
        }
    }

    @Test
    public void testIsEmailNullExist() {
        Assert.assertFalse(userRepository.isEmailExist(null));
        Assert.assertFalse(userRepository.isEmailExist(""));
        Assert.assertFalse(userRepository.isEmailExist("meow"));
    }

    @Test
    public void testIsLoginExist() {
        for (@NotNull final User user : userList) {
            @Nullable final String login = user.getLogin();
            Assert.assertTrue(userRepository.isLoginExist(login));
        }
    }

    @Test
    public void testIsLoginNullExist() {
        Assert.assertFalse(userRepository.isLoginExist(null));
        Assert.assertFalse(userRepository.isLoginExist(""));
        Assert.assertFalse(userRepository.isLoginExist("meow"));
    }

    @Test
    public void testRemove() {
        @Nullable final User user = userRepository.findOneByIndex(0);
        Assert.assertNotNull(user);
        @NotNull final String userId = user.getId();
        @Nullable final User deletedUser = userRepository.remove(user);
        Assert.assertNotNull(deletedUser);
        Assert.assertFalse(userRepository.existsById(userId));
    }

    @Test
    public void testRemoveNull() {
        Assert.assertThrows(EntityNotFoundException.class, () -> userRepository.remove(null));
    }

    @Test
    public void testRemoveAllNull() {
        int expectedCount = userRepository.getSize();
        userRepository.removeAll(null);
        int actualCount = userRepository.getSize();
        Assert.assertEquals(expectedCount, actualCount);
    }

    @Test
    public void testRemoveById() {
        int index = userList.size();
        while (index > 0) {
            @Nullable final User user = userRepository.findOneByIndex(index - 1);
            Assert.assertNotNull(user);
            @NotNull final String userId = user.getId();
            @Nullable final User deletedUser = userRepository.removeById(userId);
            Assert.assertNotNull(deletedUser);
            Assert.assertFalse(userRepository.existsById(userId));
            index--;
        }
    }

    @Test
    public void testRemoveByNullId() {
        Assert.assertThrows(EntityNotFoundException.class, () -> userRepository.removeById("meow"));
        Assert.assertThrows(EntityNotFoundException.class, () -> userRepository.removeById(""));
        Assert.assertThrows(EntityNotFoundException.class, () -> userRepository.removeById(null));
    }

    @Test
    public void testRemoveByIndex() {
        int index = userList.size();
        while (index > 0) {
            @Nullable final User deletedUser = userRepository.removeByIndex(index - 1);
            Assert.assertNotNull(deletedUser);
            @NotNull final String userId = deletedUser.getId();
            Assert.assertFalse(userRepository.existsById(userId));
            index--;
        }
    }

    @Test
    public void testRemoveByNullIndex() {
        Assert.assertThrows(IndexIncorrectException.class, () -> userRepository.removeByIndex(-1));
        Assert.assertThrows(IndexIncorrectException.class, () -> userRepository.removeByIndex(userRepository.getSize()));
        Assert.assertThrows(IndexIncorrectException.class, () -> userRepository.removeByIndex(null));
    }

    @Test
    public void setTest() {
        int expectedNumberOfEntries = 2;
        @NotNull final List<User> users = new ArrayList<>();
        @NotNull final String firstLogin = "Test1";
        @NotNull final String firstEmail = "Test1@test.com";
        @NotNull final String firstFirstName = "FirstName1";
        @NotNull final String firstLastName = "LastName1";
        @NotNull final User firstUser = new User();
        firstUser.setLogin(firstLogin);
        firstUser.setEmail(firstEmail);
        firstUser.setFirstName(firstFirstName);
        firstUser.setLastName(firstLastName);
        users.add(firstUser);
        @NotNull final String secondLogin = "Test2";
        @NotNull final String secondEmail = "Test2@test.com";
        @NotNull final String secondFirstName = "FirstName2";
        @NotNull final String secondLastName = "LastName2";
        @NotNull final User secondUser = new User();
        firstUser.setLogin(secondLogin);
        firstUser.setEmail(secondEmail);
        firstUser.setFirstName(secondFirstName);
        firstUser.setLastName(secondLastName);
        users.add(secondUser);
        @NotNull final Collection<User> addedUsers = userRepository.set(users);
        Assert.assertTrue(addedUsers.size() > 0);
        int actualNumberOfEntries = userRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

}