package t1.dkhrunina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.dto.request.task.TaskRemoveByIdRequest;
import t1.dkhrunina.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "t-remove-by-id";

    @NotNull
    private static final String DESCRIPTION = "Remove task by id.";

    @Override
    public void execute() {
        System.out.println("[Remove task by id]");
        System.out.println("Enter id: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(getToken(), id);
        getTaskEndpoint().removeTaskById(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}