package t1.dkhrunina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.api.endpoint.IAuthEndpoint;
import t1.dkhrunina.tm.api.endpoint.IUserEndpoint;
import t1.dkhrunina.tm.command.AbstractCommand;
import t1.dkhrunina.tm.exception.user.UserNotFoundException;
import t1.dkhrunina.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    public IUserEndpoint getUserEndpoint() {
        return serviceLocator.getUserEndpoint();
    }

    @NotNull
    public IAuthEndpoint getAuthEndpoint() {
        return serviceLocator.getAuthEndpoint();
    }

    protected void showUser(@Nullable final User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("Id: " + user.getId());
        System.out.println("Login: " + user.getLogin());
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}