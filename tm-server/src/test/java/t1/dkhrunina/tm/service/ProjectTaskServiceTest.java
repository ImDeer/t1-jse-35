package t1.dkhrunina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import t1.dkhrunina.tm.api.repository.IProjectRepository;
import t1.dkhrunina.tm.api.repository.ITaskRepository;
import t1.dkhrunina.tm.api.repository.IUserRepository;
import t1.dkhrunina.tm.api.service.*;
import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.exception.entity.ProjectNotFoundException;
import t1.dkhrunina.tm.exception.entity.TaskNotFoundException;
import t1.dkhrunina.tm.exception.field.IndexIncorrectException;
import t1.dkhrunina.tm.exception.field.ProjectIdEmptyException;
import t1.dkhrunina.tm.exception.field.TaskIdEmptyException;
import t1.dkhrunina.tm.model.Project;
import t1.dkhrunina.tm.model.Task;
import t1.dkhrunina.tm.model.User;
import t1.dkhrunina.tm.repository.ProjectRepository;
import t1.dkhrunina.tm.repository.TaskRepository;
import t1.dkhrunina.tm.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ProjectTaskServiceTest {

    @NotNull
    private List<Project> projectList;

    @NotNull
    private IProjectService projectService;

    @NotNull
    private ITaskService taskService;

    @NotNull
    private IProjectTaskService projectTaskService;

    @NotNull
    private User user;

    @NotNull
    private User admin;

    @Before
    public void initService() {
        @NotNull final IProjectRepository projectRepository = new ProjectRepository();
        @NotNull final IUserRepository userRepository = new UserRepository();
        @NotNull final ITaskRepository taskRepository = new TaskRepository();
        @NotNull final IPropertyService propertyService = new PropertyService();
        projectService = new ProjectService(projectRepository);
        taskService = new TaskService(taskRepository);
        projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
        @NotNull final IUserService userService = new UserService(propertyService, userRepository, projectTaskService);
        projectList = new ArrayList<>();
        user = userService.create("USER", "USER", "user@user.user");
        admin = userService.create("ADMIN", "ADMIN", Role.ADMIN);
        @NotNull final Project project1 = projectService.create(user.getId(), "project 1", "user project 1");
        @NotNull final Project project2 = projectService.create(user.getId(), "project 2", "user project 2");
        @NotNull final Project project3 = projectService.create(admin.getId(), "project 3", "admin project");
        @NotNull final Task task1 = taskService.create(user.getId(), "task 1", "project 1 task");
        task1.setProjectId(project1.getId());
        @NotNull final Task task2 = taskService.create(user.getId(), "task 2", "project 2 task");
        task2.setProjectId(project2.getId());
        @NotNull final Task task3 = taskService.create(user.getId(), "task 3", "project 1 task");
        task3.setProjectId(project1.getId());
        @NotNull final Task task4 = taskService.create(admin.getId(), "task 4", "project 3 task");
        task4.setProjectId(project3.getId());
        projectList.add(project1);
        projectList.add(project2);
        projectList.add(project3);
    }

    @Test
    public void testBindTaskToProject() {
        @NotNull final List<Project> projects = projectService.findAll(user.getId());
        Assert.assertTrue(projects.size() > 0);
        @NotNull final Project project = projects.get(0);
        int expectedNumberOfEntries = taskService.findAllByProjectId(user.getId(), project.getId()).size() + 1;
        @NotNull final Task newTask = taskService.create(user.getId(), "Test name", "Test description");
        @NotNull final Task boundTask = projectTaskService.bindTaskToProject(user.getId(), project.getId(), newTask.getId());
        Assert.assertNotNull(boundTask);
        int newNumberOfEntries = taskService.findAllByProjectId(user.getId(), project.getId()).size();
        Assert.assertEquals(expectedNumberOfEntries, newNumberOfEntries);
    }

    @Test
    public void testBindTaskToProjectProjectIdNull() {
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.bindTaskToProject(user.getId(), null, "TASK_ID"));
    }

    @Test
    public void testBindTaskToProjectProjectIdEmpty() {
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.bindTaskToProject(user.getId(), "", "TASK_ID"));
    }

    @Test
    public void testBindTaskToProjectTaskIdNull() {
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.bindTaskToProject(user.getId(), "PROJECT_ID", null));
    }

    @Test
    public void testBindTaskToProjectTaskIdEmpty() {
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.bindTaskToProject(user.getId(), "PROJECT_ID", ""));
    }

    @Test
    public void testBindTaskToProjectProjectNotFound() {
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.bindTaskToProject(user.getId(), "PROJECT_ID", "123321"));
    }

    @Test
    public void testBindTaskToProjectTaskNotFound() {
        @NotNull final String projectId = projectList.get(0).getId();
        Assert.assertThrows(TaskNotFoundException.class, () -> projectTaskService.bindTaskToProject(user.getId(), projectId, "123321"));
    }

    @Test
    public void testRemoveProjectById() {
        @NotNull final List<Project> projects = projectList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        Assert.assertTrue(projects.size() > 0);
        for (@NotNull final Project project : projects) {
            projectTaskService.removeProjectById(user.getId(), project.getId());
        }
        @NotNull final List<Project> projectsAfterRemoving = projectService.findAll(user.getId());
        Assert.assertEquals(0, projectsAfterRemoving.size());
        @NotNull final List<Task> tasksAfterRemoving = taskService.findAll(user.getId());
        Assert.assertEquals(0, tasksAfterRemoving.size());
    }

    @Test
    public void testRemoveProjectByIdProjectIdNull() {
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.removeProjectById(user.getId(), null));
    }

    @Test
    public void testRemoveProjectByIdProjectIdEmpty() {
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.removeProjectById(user.getId(), ""));
    }

    @Test
    public void testRemoveProjectByIdProjectNotFound() {
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.removeProjectById(user.getId(), "123321"));
    }

    @Test
    public void testRemoveProjectByIndex() {
        @NotNull final List<Project> projects = projectList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        Assert.assertTrue(projects.size() > 0);
        int index = projects.size() - 1;
        while (index >= 0) {
            projectTaskService.removeProjectByIndex(user.getId(), index);
            index--;
        }
        @NotNull final List<Project> projectsAfterRemoving = projectService.findAll(user.getId());
        Assert.assertEquals(0, projectsAfterRemoving.size());
        @NotNull final List<Task> tasksAfterRemoving = taskService.findAll(user.getId());
        Assert.assertEquals(0, tasksAfterRemoving.size());
    }

    @Test
    public void testRemoveProjectByIndexNull() {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectTaskService.removeProjectByIndex(user.getId(), null));
    }

    @Test
    public void testRemoveProjectByIndexNegative() {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectTaskService.removeProjectByIndex(user.getId(), -1));
    }

    @Test
    public void testRemoveProjectByIndexInvalid() {
        Assert.assertThrows(IndexIncorrectException.class, () -> projectTaskService.removeProjectByIndex(user.getId(), projectList.size() + 1));
    }

    @Test
    public void testClearProject() {
        int numberOfProjects = projectService.getSize(user.getId());
        Assert.assertTrue(numberOfProjects > 0);
        int numberOfTasks = taskService.getSize(user.getId());
        Assert.assertTrue(numberOfTasks > 0);
        projectTaskService.removeAllByUserId(user.getId());
        int numberOfProjectsAfterRemoving = projectService.getSize(user.getId());
        Assert.assertEquals(0, numberOfProjectsAfterRemoving);
        int numberOfTasksAfterRemoving = taskService.getSize(user.getId());
        Assert.assertEquals(0, numberOfTasksAfterRemoving);
    }

    @Test
    public void testUnbindTaskFromProject() {
        @NotNull final List<Project> projects = projectService.findAll(user.getId());
        Assert.assertTrue(projects.size() > 0);
        @NotNull final Project project = projects.get(0);
        @NotNull final List<Task> tasks = taskService.findAllByProjectId(user.getId(), project.getId());
        @NotNull final String taskForUnbindId = tasks.get(0).getId();
        @Nullable final Task boundTask = projectTaskService.bindTaskToProject(user.getId(), project.getId(), taskForUnbindId);
        Assert.assertNotNull(boundTask);
        @Nullable Task unboundTask = projectTaskService.unbindTaskFromProject(user.getId(), boundTask.getProjectId(), boundTask.getId());
        Assert.assertNotNull(unboundTask);
        unboundTask = taskService.findOneById(unboundTask.getId());
        Assert.assertNotNull(unboundTask);
        Assert.assertNull(unboundTask.getProjectId());
    }

    @Test
    public void testUnbindTaskProjectIdNull() {
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(user.getId(), null, "TASK_ID"));
    }

    @Test
    public void testUnbindTaskProjectIdEmpty() {
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(user.getId(), "", "TASK_ID"));
    }

    @Test
    public void testUnbindTaskTaskIdNull() {
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(user.getId(), "PROJECT_ID", null));
    }

    @Test
    public void testUnbindTaskTaskIdEmpty() {
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(user.getId(), "PROJECT_ID", ""));
    }

    @Test
    public void testUnbindTaskProjectNotFound() {
        Assert.assertThrows(ProjectNotFoundException.class, ()->projectTaskService.unbindTaskFromProject(user.getId(), "PROJECT_ID", "123321"));
    }

    @Test
    public void testUnbindTaskTaskNotFound() {
        @NotNull final String projectId = projectList.get(0).getId();
        Assert.assertThrows(TaskNotFoundException.class, ()->projectTaskService.unbindTaskFromProject(user.getId(), projectId, "123321"));
    }

}