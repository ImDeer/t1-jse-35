package t1.dkhrunina.tm.repository;

import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.api.repository.IUserRepository;
import t1.dkhrunina.tm.model.User;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public boolean isEmailExist(@Nullable final String email) {
        if (email == null) return false;
        return findAll()
                .stream()
                .anyMatch(m -> email.equals(m.getEmail()));
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) {
        if (login == null) return false;
        return findAll()
                .stream()
                .anyMatch(m -> login.equals(m.getLogin()));
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) {
        if (email == null) return null;
        return findAll()
                .stream()
                .filter(m -> email.equals(m.getEmail()))
                .findFirst().orElse(null);
    }

    @Nullable
    @Override
    public User findById(@Nullable final String id) {
        if (id == null) return null;
        return findAll()
                .stream()
                .filter(m -> id.equals(m.getId()))
                .findFirst().orElse(null);
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null||login.isEmpty()) return null;
        return findAll()
                .stream()
                .filter(m -> login.equals(m.getLogin()))
                .findFirst().orElse(null);
    }

}