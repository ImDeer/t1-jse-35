package t1.dkhrunina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.dto.request.task.TaskBindToProjectRequest;
import t1.dkhrunina.tm.util.TerminalUtil;

public final class TaskBindToProjectCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "t-bind";

    @NotNull
    private static final String DESCRIPTION = "Bind task to project by project id.";

    @Override
    public void execute() {
        System.out.println("[Bind task to project]");
        System.out.println("Enter project id: ");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("Enter task id: ");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(getToken(), projectId, taskId);
        getTaskEndpoint().bindTaskToProject(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}