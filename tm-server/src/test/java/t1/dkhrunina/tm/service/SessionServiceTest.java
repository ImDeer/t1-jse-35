package t1.dkhrunina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import t1.dkhrunina.tm.api.repository.IProjectRepository;
import t1.dkhrunina.tm.api.repository.ISessionRepository;
import t1.dkhrunina.tm.api.repository.ITaskRepository;
import t1.dkhrunina.tm.api.repository.IUserRepository;
import t1.dkhrunina.tm.api.service.IProjectTaskService;
import t1.dkhrunina.tm.api.service.IPropertyService;
import t1.dkhrunina.tm.api.service.ISessionService;
import t1.dkhrunina.tm.api.service.IUserService;
import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.exception.entity.EntityNotFoundException;
import t1.dkhrunina.tm.exception.field.IdEmptyException;
import t1.dkhrunina.tm.exception.field.IndexIncorrectException;
import t1.dkhrunina.tm.model.Session;
import t1.dkhrunina.tm.model.User;
import t1.dkhrunina.tm.repository.ProjectRepository;
import t1.dkhrunina.tm.repository.SessionRepository;
import t1.dkhrunina.tm.repository.TaskRepository;
import t1.dkhrunina.tm.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SessionServiceTest {

    @NotNull
    private List<Session> sessionList;

    @NotNull
    private ISessionService sessionService;

    @NotNull
    private User user;

    @NotNull
    private User admin;

    @Before
    public void initService() {
        @NotNull final IProjectRepository projectRepository = new ProjectRepository();
        @NotNull final IUserRepository userRepository = new UserRepository();
        @NotNull final ITaskRepository taskRepository = new TaskRepository();
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository();
        @NotNull final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
        @NotNull final IUserService userService = new UserService(propertyService, userRepository, projectTaskService);
        sessionService = new SessionService(sessionRepository);
        user = userService.create("USER", "USER", "user@user.user");
        admin = userService.create("ADMIN", "ADMIN", Role.ADMIN);
        @NotNull final Session session1 = new Session();
        session1.setUserId(user.getId());
        session1.setRole(user.getRole());
        @NotNull final Session session2 = new Session();
        session2.setUserId(admin.getId());
        session2.setRole(admin.getRole());
        sessionList = new ArrayList<>();
        sessionList.add(session1);
        sessionList.add(session2);
        sessionService.add(sessionList);
    }

    @Test
    public void testAdd() {
        int expectedNumberOfEntries = sessionService.getSize() + 1;
        @NotNull final Session session = new Session();
        session.setUserId("45");
        session.setRole(Role.USUAL);
        sessionService.add(session);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize());
    }

    @Test
    public void testAddForUser() {
        int expectedNumberOfEntries = sessionService.getSize(user.getId()) + 1;
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setRole(user.getRole());
        sessionService.add(user.getId(), session);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize(user.getId()));
    }

    @Test
    public void testAddForUserNull() {
        int expectedNumberOfEntries = sessionService.getSize(user.getId());
        @Nullable final Session session = sessionService.add(user.getId(), null);
        Assert.assertNull(session);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize(user.getId()));
    }

    @Test
    public void testAddCollection() {
        int expectedNumberOfEntries = sessionService.getSize() + 2;
        @NotNull final List<Session> sessionList = new ArrayList<>();
        @NotNull final Session firstSession = new Session();
        firstSession.setUserId("45");
        firstSession.setRole(Role.USUAL);
        sessionList.add(firstSession);
        @NotNull final Session secondSession = new Session();
        secondSession.setUserId("45");
        secondSession.setRole(Role.USUAL);
        sessionList.add(secondSession);
        sessionService.add(sessionList);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize());
    }

    @Test
    public void testClear() {
        int expectedNumberOfEntries = 0;
        Assert.assertTrue(sessionService.getSize() > 0);
        sessionService.clear();
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize());
    }

    @Test
    public void testClearForUser() {
        int expectedNumberOfEntries = 0;
        Assert.assertTrue(sessionService.getSize(user.getId()) > 0);
        sessionService.clear(user.getId());
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize(user.getId()));
    }

    @Test
    public void testSet() {
        int expectedNumberOfEntries = 2;
        @NotNull final List<Session> sessionList = new ArrayList<>();
        @NotNull final Session firstSession = new Session();
        firstSession.setUserId("45");
        firstSession.setRole(Role.USUAL);
        sessionList.add(firstSession);
        @NotNull final Session secondSession = new Session();
        secondSession.setUserId("45");
        secondSession.setRole(Role.USUAL);
        sessionList.add(secondSession);
        sessionService.set(sessionList);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize());
    }

    @Test
    public void testExistById() {
        for (@NotNull final Session session : sessionList) {
            final boolean isExist = sessionService.existsById(session.getId());
            Assert.assertTrue(isExist);
        }
    }

    @Test
    public void testExistByIdFalse() {
        final boolean isExist = sessionService.existsById("123321");
        Assert.assertFalse(isExist);
    }

    @Test
    public void testExistByIdForUser() {
        @NotNull final List<Session> sessionsForTestUser = sessionList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final Session session : sessionsForTestUser) {
            final boolean isExist = sessionService.existsById(user.getId(), session.getId());
            Assert.assertTrue(isExist);
        }
    }

    @Test
    public void testExistByIdForUserFalse() {
        final boolean isExist = sessionService.existsById("45", "123321");
        Assert.assertFalse(isExist);
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Session> sessions = sessionService.findAll();
        Assert.assertEquals(sessionList, sessions);
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<Session> sessionsForTestUser = sessionList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull final List<Session> sessions = sessionService.findAll(user.getId());
        Assert.assertEquals(sessionsForTestUser, sessions);
    }

    @Test
    public void testFindOneById() {
        @Nullable Session foundSession;
        for (@NotNull final Session session : sessionList) {
            foundSession = sessionService.findOneById(session.getId());
            Assert.assertNotNull(foundSession);
        }
    }

    @Test
    public void testFindOneByIdNull() {
        @Nullable final Session foundSession = sessionService.findOneById(null);
        Assert.assertNull(foundSession);
    }

    @Test
    public void testFindOneByIdEmpty() {
        @Nullable final Session foundSession = sessionService.findOneById("");
        Assert.assertNull(foundSession);
    }

    @Test
    public void testFindOneByIdForUser() {
        @Nullable Session foundSession;
        @NotNull final List<Session> sessionsForTestUser = sessionList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final Session session : sessionsForTestUser) {
            foundSession = sessionService.findOneById(user.getId(), session.getId());
            Assert.assertNotNull(foundSession);
        }
    }

    @Test
    public void testFindOneByIdForUserNull() {
        @Nullable final Session foundSession = sessionService.findOneById(user.getId(), null);
        Assert.assertNull(foundSession);
    }

    @Test
    public void testFindOneByIdForUserEmpty() {
        @Nullable final Session foundSession = sessionService.findOneById(user.getId(), "");
        Assert.assertNull(foundSession);
    }

    @Test
    public void testFindOneByIndex() {
        for (int i = 0; i < sessionList.size(); i++) {
            @Nullable final Session session = sessionService.findOneByIndex(i);
            Assert.assertNotNull(session);
        }
    }

    @Test
    public void testFindOneByIndexIncorrect() {
        int index = sessionService.getSize() + 1;
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.findOneByIndex(index));
    }

    @Test
    public void testFindOneByIndexNull() {
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.findOneByIndex(null));
    }

    @Test
    public void testFindOneByIndexNegative() {
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.findOneByIndex(-1));
    }

    @Test
    public void testFindOneByIndexForUser() {
        @NotNull final List<Session> sessionsForTestUser = sessionList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 0; i < sessionsForTestUser.size(); i++) {
            @Nullable final Session session = sessionService.findOneByIndex(user.getId(), i);
            Assert.assertNotNull(session);
        }
    }

    @Test
    public void testFindOneByIndexIncorrectForUser() {
        int index = sessionService.getSize(user.getId()) + 1;
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.findOneByIndex(user.getId(), index));
    }

    @Test
    public void testFindOneByIndexNullForUser() {
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.findOneByIndex(user.getId(), null));
    }

    @Test
    public void testFindOneByIndexNegativeForUser() {
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.findOneByIndex(user.getId(), -1));
    }

    @Test
    public void testGetSize() {
        int expectedSize = sessionList.size();
        int actualSize = sessionService.getSize();
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void testGetSizeForUser() {
        int expectedSize = (int) sessionList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .count();
        int actualSize = sessionService.getSize(user.getId());
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void testRemove() {
        for (@NotNull final Session session : sessionList) {
            @NotNull final String sessionId = session.getId();
            @Nullable final Session deletedSession = sessionService.remove(session);
            Assert.assertNotNull(deletedSession);
            @Nullable final Session deletedSessionInRepository = sessionService.findOneById(sessionId);
            Assert.assertNull(deletedSessionInRepository);
        }
    }

    @Test
    public void testRemoveEntityNotFound() {
        @NotNull final Session session = new Session();
        Assert.assertThrows(EntityNotFoundException.class, () -> sessionService.remove(session));
    }

    @Test
    public void testRemoveEntityNull() {
        Assert.assertThrows(EntityNotFoundException.class, () -> sessionService.remove(null));
    }

    @Test
    public void testRemoveAll() {
        int expectedNumberOfEntries = 0;
        @NotNull final List<Session> sessionsForTestUser = sessionList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        sessionService.removeAll(sessionsForTestUser);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize(user.getId()));
    }

    @Test
    public void testRemoveById() {
        for (@NotNull final Session session : sessionList) {
            @NotNull final String sessionId = session.getId();
            @Nullable final Session deletedSession = sessionService.removeById(sessionId);
            Assert.assertNotNull(deletedSession);
            @Nullable final Session deletedSessionInRepository = sessionService.findOneById(sessionId);
            Assert.assertNull(deletedSessionInRepository);
        }
    }

    @Test
    public void testRemoveByIdNull() {
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.removeById(null));
    }

    @Test
    public void testRemoveByIdEmpty() {
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.removeById(""));
    }

    @Test
    public void testRemoveByIdNotFound() {
        Assert.assertThrows(EntityNotFoundException.class, () -> sessionService.removeById("meow"));
    }

    @Test
    public void testRemoveByIdForUser() {
        @NotNull final List<Session> sessionsForTestUser = sessionList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final Session session : sessionsForTestUser) {
            @NotNull final String sessionId = session.getId();
            @Nullable final Session deletedSession = sessionService.removeById(user.getId(), sessionId);
            Assert.assertNotNull(deletedSession);
            @Nullable final Session deletedSessionInRepository = sessionService.findOneById(user.getId(), sessionId);
            Assert.assertNull(deletedSessionInRepository);
        }
    }

    @Test
    public void testRemoveByIdNullForUser() {
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.removeById(user.getId(), null));
    }

    @Test
    public void testRemoveByIdEmptyForUser() {
        Assert.assertThrows(IdEmptyException.class, () -> sessionService.removeById(user.getId(), ""));
    }

    @Test
    public void testRemoveByIdNotFoundForUser() {
        Assert.assertThrows(EntityNotFoundException.class, () -> sessionService.removeById(user.getId(), "meow"));
    }

    @Test
    public void testRemoveByIndex() {
        int index = sessionList.size();
        while (index > 0) {
            @Nullable final Session deletedSession = sessionService.removeByIndex(index - 1);
            Assert.assertNotNull(deletedSession);
            @NotNull final String sessionId = deletedSession.getId();
            @Nullable final Session deletedSessionInRepository = sessionService.findOneById(sessionId);
            Assert.assertNull(deletedSessionInRepository);
            index--;
        }
    }

    @Test
    public void testRemoveByIndexInvalid() {
        int index = sessionList.size() + 1;
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.removeByIndex(index));
    }

    @Test
    public void testRemoveByIndexNull() {
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.removeByIndex(null));
    }

    @Test
    public void testRemoveByIndexNegative() {
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.removeByIndex(-1));
    }

    @Test
    public void testRemoveByIndexForUser() {
        int index = (int) sessionList
                .stream()
                .filter(m -> user.getId().equals(m.getUserId()))
                .count();
        while (index > 0) {
            @Nullable final Session deletedSession = sessionService.removeByIndex(user.getId(), index - 1);
            Assert.assertNotNull(deletedSession);
            @NotNull final String sessionId = deletedSession.getId();
            @Nullable final Session deletedSessionInRepository = sessionService.findOneById(user.getId(), sessionId);
            Assert.assertNull(deletedSessionInRepository);
            index--;
        }
    }

    @Test
    public void testRemoveByIndexInvalidForUser() {
        int index = sessionList.size() + 1;
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.removeByIndex(user.getId(), index));
    }

    @Test
    public void testRemoveByIndexNullForUser() {
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.removeByIndex(user.getId(), null));
    }

    @Test
    public void testRemoveByIndexNegaiveForUser() {
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionService.removeByIndex(user.getId(), -1));
    }

}