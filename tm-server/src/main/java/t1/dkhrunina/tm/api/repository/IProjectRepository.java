package t1.dkhrunina.tm.api.repository;

import t1.dkhrunina.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

}