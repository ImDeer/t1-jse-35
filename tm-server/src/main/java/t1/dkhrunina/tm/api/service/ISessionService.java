package t1.dkhrunina.tm.api.service;

import t1.dkhrunina.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {

}
