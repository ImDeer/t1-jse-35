package t1.dkhrunina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.dto.request.user.UserUpdateProfileRequest;
import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "u-update-profile";

    @NotNull
    private static final String DESCRIPTION = "Update current user profile.";

    @Override
    public void execute() {
        System.out.println("[Update user profile]");
        System.out.println("Enter first name: ");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter middle name: ");
        @NotNull final String middleName = TerminalUtil.nextLine();
        System.out.println("Enter last name: ");
        @NotNull final String lastName = TerminalUtil.nextLine();
        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(getToken(), firstName, middleName, lastName);
        getUserEndpoint().updateUserProfile(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}