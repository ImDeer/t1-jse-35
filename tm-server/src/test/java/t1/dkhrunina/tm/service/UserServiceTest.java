package t1.dkhrunina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import t1.dkhrunina.tm.api.repository.IProjectRepository;
import t1.dkhrunina.tm.api.repository.ISessionRepository;
import t1.dkhrunina.tm.api.repository.ITaskRepository;
import t1.dkhrunina.tm.api.repository.IUserRepository;
import t1.dkhrunina.tm.api.service.*;
import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.exception.entity.EntityNotFoundException;
import t1.dkhrunina.tm.exception.field.*;
import t1.dkhrunina.tm.exception.user.EmailExistsException;
import t1.dkhrunina.tm.exception.user.LoginExistsException;
import t1.dkhrunina.tm.exception.user.UserNotFoundException;
import t1.dkhrunina.tm.model.User;
import t1.dkhrunina.tm.repository.ProjectRepository;
import t1.dkhrunina.tm.repository.SessionRepository;
import t1.dkhrunina.tm.repository.TaskRepository;
import t1.dkhrunina.tm.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

public class UserServiceTest {

    @NotNull
    private List<User> userList;

    @NotNull
    private IUserService userService;

    @NotNull
    private IAuthService authService;

    @Before
    public void initService() {
        @NotNull final IProjectRepository projectRepository = new ProjectRepository();
        @NotNull final IUserRepository userRepository = new UserRepository();
        @NotNull final ITaskRepository taskRepository = new TaskRepository();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository();
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
        userService = new UserService(propertyService, userRepository, projectTaskService);
        @NotNull final ISessionService sessionService = new SessionService(sessionRepository);
        authService = new AuthService(propertyService, userService, sessionService);
        @NotNull final User user = userService.create("USER", "USER", "user@user.user");
        @NotNull final User admin = userService.create("ADMIN", "ADMIN", Role.ADMIN);
        admin.setEmail("admin@admin.admin");
        userList = new ArrayList<>();
        userList.addAll(userService.findAll());
    }

    @Test
    public void testAdd() {
        int expectedNumberOfEntries = userService.getSize() + 1;
        @NotNull final User user = new User();
        user.setLogin("TEST_LOGIN");
        user.setFirstName("TEST_FIRSTNAME");
        user.setLastName("TEST_LASTNAME");
        user.setEmail("TEST@TEST.TEST");
        user.setRole(Role.USUAL);
        userService.add(user);
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
    }

    @Test
    public void testAddCollection() {
        int expectedNumberOfEntries = userService.getSize() + 2;
        @NotNull final List<User> userList = new ArrayList<>();
        @NotNull final User firstUser = new User();
        firstUser.setLogin("TEST_LOGIN1");
        firstUser.setFirstName("TEST_FIRSTNAME1");
        firstUser.setLastName("TEST_LASTNAME1");
        firstUser.setEmail("TEST1@TEST.TEST");
        firstUser.setRole(Role.USUAL);
        userList.add(firstUser);
        @NotNull final User secondUser = new User();
        firstUser.setLogin("TEST_LOGIN2");
        firstUser.setFirstName("TEST_FIRSTNAME2");
        firstUser.setLastName("TEST_LASTNAME2");
        firstUser.setEmail("TEST2@TEST.TEST");
        firstUser.setRole(Role.USUAL);
        userList.add(secondUser);
        userService.add(userList);
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
    }

    @Test
    public void testCreate() {
        @NotNull final String login = "LOGIN";
        @NotNull final String password = "PASS";
        @NotNull final User user = userService.create(login, password);
        @NotNull final String userId = user.getId();
        @Nullable final User newUser = userService.findOneById(userId);
        Assert.assertNotNull(newUser);
        Assert.assertEquals(login, newUser.getLogin());
    }

    @Test
    public void testCreateLoginEmpty() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create("", "PASS"));
    }

    @Test
    public void testCreateLoginExists() {
        Assert.assertThrows(LoginExistsException.class, () -> userService.create("USER", "PASS"));
    }

    @Test
    public void testCreatePasswordEmpty() {
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("LOGIN", ""));
    }

    @Test
    public void testCreateWithEmail() {
        @NotNull final String login = "LOGIN";
        @NotNull final String password = "PASS";
        @NotNull final String email = "EMAIL";
        @NotNull final User user = userService.create(login, password, email);
        @NotNull final String userId = user.getId();
        @Nullable final User newUser = userService.findOneById(userId);
        Assert.assertNotNull(newUser);
        Assert.assertEquals(login, newUser.getLogin());
        Assert.assertEquals(email, newUser.getEmail());
    }

    @Test
    public void testCreateWithEmailLoginEmpty() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create("", "PASS", "EMAIL"));
    }

    @Test
    public void testCreateWithEmailLoginExists() {
        Assert.assertThrows(LoginExistsException.class, () -> userService.create("USER", "PASS", "EMAIL"));
    }

    @Test
    public void testCreateWithEmailPasswordEmpty() {
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("LOGIN", "", "EMAIL"));
    }

    @Test
    public void testCreateWithEmailEmailExists() {
        Assert.assertThrows(EmailExistsException.class, () -> userService.create("LOGIN", "PASS", "user@user.user"));
    }

    @Test
    public void testCreateWithRole() {
        @NotNull final String login = "LOGIN";
        @NotNull final String password = "PASS";
        @NotNull final Role role = Role.USUAL;
        @NotNull final User user = userService.create(login, password, role);
        @NotNull final String userId = user.getId();
        @Nullable final User newUser = userService.findOneById(userId);
        Assert.assertNotNull(newUser);
        Assert.assertEquals(login, newUser.getLogin());
        Assert.assertEquals(role, newUser.getRole());
    }

    @Test
    public void testCreateWithRoleLoginEmpty() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create("", "PASS", Role.USUAL));
    }

    @Test
    public void testCreateWithRoleLoginExists() {
        Assert.assertThrows(LoginExistsException.class, () -> userService.create("USER", "PASS", Role.USUAL));
    }

    @Test
    public void testCreateWithRolePasswordEmpty() {
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("LOGIN", "", Role.USUAL));
    }

    @Test
    public void testClear() {
        int expectedNumberOfEntries = 0;
        Assert.assertTrue(userService.getSize() > 0);
        userService.clear();
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
    }

    @Test
    public void testSet() {
        int expectedNumberOfEntries = 2;
        @NotNull final List<User> userList = new ArrayList<>();
        @NotNull final User firstUser = new User();
        firstUser.setLogin("TEST_LOGIN1");
        firstUser.setFirstName("TEST_FIRSTNAME1");
        firstUser.setLastName("TEST_LASTNAME1");
        firstUser.setEmail("TEST1@TEST.TEST");
        firstUser.setRole(Role.USUAL);
        userList.add(firstUser);
        @NotNull final User secondUser = new User();
        firstUser.setLogin("TEST_LOGIN2");
        firstUser.setFirstName("TEST_FIRSTNAME2");
        firstUser.setLastName("TEST_LASTNAME2");
        firstUser.setEmail("TEST2@TEST.TEST");
        firstUser.setRole(Role.USUAL);
        userList.add(secondUser);
        userService.set(userList);
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
    }

    @Test
    public void testExistById() {
        for (@NotNull final User user : userList) {
            final boolean isExist = userService.existsById(user.getId());
            Assert.assertTrue(isExist);
        }
    }

    @Test
    public void testExistByIdFalse() {
        final boolean isExist = userService.existsById("123321");
        Assert.assertFalse(isExist);
    }

    @Test
    public void testFindAll() {
        @NotNull final List<User> users = userService.findAll();
        Assert.assertEquals(userList, users);
    }

    @Test
    public void testFindByLogin() {
        Assert.assertTrue(userList.size() > 0);
        for (@NotNull final User user : userList) {
            @Nullable final User foundUser = userService.findByLogin(user.getLogin());
            Assert.assertNotNull(foundUser);
        }
    }

    @Test
    public void testFindByLoginEmpty() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(""));
    }

    @Test
    public void testFindByLoginNull() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(null));
    }

    @Test
    public void testFindByEmail() {
        Assert.assertTrue(userList.size() > 0);
        for (@NotNull final User user : userList) {
            @Nullable final User foundUser = userService.findByEmail(user.getEmail());
            Assert.assertNotNull(foundUser);
        }
    }

    @Test
    public void testFindOneById() {
        @Nullable User foundUser;
        for (@NotNull final User user : userList) {
            foundUser = userService.findOneById(user.getId());
            Assert.assertNotNull(foundUser);
        }
    }

    @Test
    public void testFindOneByIdNull() {
        @Nullable final User foundUser = userService.findOneById(null);
        Assert.assertNull(foundUser);
    }

    @Test
    public void testFindOneByIdEmpty() {
        @Nullable final User foundUser = userService.findOneById("");
        Assert.assertNull(foundUser);
    }

    @Test
    public void testFindOneByIndex() {
        for (int i = 0; i < userList.size(); i++) {
            @Nullable final User user = userService.findOneByIndex(i);
            Assert.assertNotNull(user);
        }
    }

    @Test
    public void testFindOneByIndexInvalid() {
        int index = userService.getSize() + 1;
        Assert.assertThrows(IndexIncorrectException.class, () -> userService.findOneByIndex(index));
    }

    @Test
    public void testFindOneByIndexNull() {
        Assert.assertThrows(IndexIncorrectException.class, () -> userService.findOneByIndex(null));
    }

    @Test
    public void testFindOneByIndexNegative() {
        Assert.assertThrows(IndexIncorrectException.class, () -> userService.findOneByIndex(-1));
    }

    @Test
    public void teatGetSize() {
        int expectedSize = userList.size();
        int actualSize = userService.getSize();
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void testIsLoginExist() {
        boolean isExist = userService.isLoginExist("ADMIN");
        Assert.assertTrue(isExist);
    }

    @Test
    public void testIsLoginExistFalse() {
        boolean isExist = userService.isLoginExist("meow");
        Assert.assertFalse(isExist);
    }

    @Test
    public void testIsLoginExistNull() {
        boolean isExist = userService.isLoginExist(null);
        Assert.assertFalse(isExist);
    }

    @Test
    public void testIsLoginExistEmpty() {
        boolean isExist = userService.isLoginExist("");
        Assert.assertFalse(isExist);
    }

    @Test
    public void testIsEmailExist() {
        boolean isExist = userService.isEmailExist("user@user.user");
        Assert.assertTrue(isExist);
    }

    @Test
    public void testIsEmailExistFalse() {
        boolean isExist = userService.isEmailExist("meow@meow.meow");
        Assert.assertFalse(isExist);
    }

    @Test
    public void testIsEmailExistNull() {
        boolean isExist = userService.isEmailExist(null);
        Assert.assertFalse(isExist);
    }

    @Test
    public void testIsEmailExistEmpty() {
        boolean isExist = userService.isEmailExist("");
        Assert.assertFalse(isExist);
    }

    @Test
    public void testLockUserByLogin() {
        userService.lockUserByLogin("USER");
        @Nullable final User user = userService.findByLogin("USER");
        Assert.assertNotNull(user);
        Assert.assertEquals(true, user.getLocked());
    }

    @Test
    public void testLockUserByLoginNull() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(null));
    }

    @Test
    public void testLockUserByLoginEmpty() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(""));
    }

    @Test
    public void testLockUserByLoginInvalid() {
        Assert.assertThrows(UserNotFoundException.class, () -> userService.lockUserByLogin("meow"));
    }

    @Test
    public void testRemoveByLogin() {
        @Nullable final User user = userService.findByLogin("TEST");
        Assert.assertNotNull(user);
        @Nullable User deletedUser = userService.removeByLogin("TEST");
        Assert.assertNotNull(deletedUser);
        deletedUser = userService.findByLogin("TEST");
        Assert.assertNull(deletedUser);
    }

    @Test
    public void testRemoveByLoginNull() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(null));
    }

    @Test
    public void testRemoveByLoginEmpty() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(""));
    }

    @Test
    public void testRemoveByLoginInvalid() {
        Assert.assertThrows(EntityNotFoundException.class, () -> userService.removeByLogin("meow"));
    }

    @Test
    public void testRemoveByEmail() {
        @Nullable final User user = userService.findByEmail("user@user.user");
        Assert.assertNotNull(user);
        @Nullable User deletedUser = userService.removeByEmail("user@user.user");
        Assert.assertNotNull(deletedUser);
        deletedUser = userService.findByEmail("user@user.user");
        Assert.assertNull(deletedUser);
    }

    @Test
    public void testRemoveByEmailNull() {
        Assert.assertThrows(EmailEmptyException.class, () -> userService.removeByEmail(null));
    }

    @Test
    public void testRemoveByEmailEmpty() {
        Assert.assertThrows(EmailEmptyException.class, () -> userService.removeByEmail(""));
    }

    @Test
    public void testRemoveByEmailNotFound() {
        Assert.assertThrows(EntityNotFoundException.class, () -> userService.removeByEmail("123"));
    }

    @Test
    public void testSetPassword() {
        @Nullable final User user = userService.findByLogin("USER");
        Assert.assertNotNull(user);
        @Nullable final User updatedUser = userService.setPassword(user.getId(), "NEW_PASS");
        Assert.assertNotNull(updatedUser);
        @Nullable final String token = authService.login("USER", "NEW_PASS");
        Assert.assertNotNull(token);
    }

    @Test
    public void testSetPasswordIdNull() {
        Assert.assertThrows(IdEmptyException.class, () -> userService.setPassword(null, "NEW_PASS"));
    }

    @Test
    public void testSetPasswordIdEmpty() {
        Assert.assertThrows(IdEmptyException.class, () -> userService.setPassword("", "NEW_PASS"));
    }

    @Test
    public void testSetPasswordNull() {
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword("ID", null));
    }

    @Test
    public void testSetPasswordEmpty() {
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword("ID", ""));
    }

    @Test
    public void testSetPasswordNotFound() {
        Assert.assertThrows(UserNotFoundException.class, () -> userService.setPassword("meow", "NEW_PASS"));
    }

    @Test
    public void testRemove() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @Nullable final User deletedUser = userService.remove(user);
            Assert.assertNotNull(deletedUser);
            @Nullable final User deletedUserInRepository = userService.findOneById(userId);
            Assert.assertNull(deletedUserInRepository);
        }
    }

    @Test
    public void testRemoveNotFound() {
        @NotNull final User user = new User();
        Assert.assertThrows(EntityNotFoundException.class, () -> userService.remove(user));
    }

    @Test
    public void testRemoveNull() {
        Assert.assertThrows(EntityNotFoundException.class, () -> userService.remove(null));
    }

    @Test
    public void testRemoveById() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @Nullable final User deletedUser = userService.removeById(userId);
            Assert.assertNotNull(deletedUser);
            @Nullable final User deletedUserInRepository = userService.findOneById(userId);
            Assert.assertNull(deletedUserInRepository);
        }
    }

    @Test
    public void testRemoveByIdNull() {
        Assert.assertThrows(IdEmptyException.class, () -> userService.removeById(null));
    }

    @Test
    public void testRemoveByIdEmpty() {
        Assert.assertThrows(IdEmptyException.class, () -> userService.removeById(""));
    }

    @Test
    public void testRemoveByIdNotFound() {
        Assert.assertThrows(EntityNotFoundException.class, () -> userService.removeById("meow"));
    }

    @Test
    public void testRemoveByIndex() {
        int index = userList.size();
        while (index > 0) {
            @Nullable final User deletedUser = userService.removeByIndex(index - 1);
            Assert.assertNotNull(deletedUser);
            @NotNull final String userId = deletedUser.getId();
            @Nullable final User deletedUserInRepository = userService.findOneById(userId);
            Assert.assertNull(deletedUserInRepository);
            index--;
        }
    }

    @Test
    public void testRemoveByIndexInvalid() {
        int index = userList.size() + 1;
        Assert.assertThrows(IndexIncorrectException.class, () -> userService.removeByIndex(index));
    }

    @Test
    public void testRemoveByIndexNull() {
        Assert.assertThrows(IndexIncorrectException.class, () -> userService.removeByIndex(null));
    }

    @Test
    public void testRemoveByIndexNegative() {
        Assert.assertThrows(IndexIncorrectException.class, () -> userService.removeByIndex(-1));
    }

    @Test
    public void testUnlockUserByLogin() {
        userService.lockUserByLogin("USER");
        @Nullable final User lockedUser = userService.findByLogin("USER");
        Assert.assertNotNull(lockedUser);
        Assert.assertEquals(true, lockedUser.getLocked());
        userService.unlockUserByLogin("USER");
        @Nullable final User unlockedUser = userService.findByLogin("USER");
        Assert.assertNotNull(unlockedUser);
        Assert.assertEquals(false, unlockedUser.getLocked());
    }

    @Test
    public void testUnlockUserByLoginNull() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.unlockUserByLogin(null));
    }

    @Test
    public void testUnlockUserByLoginEmpty() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.unlockUserByLogin(""));
    }

    @Test
    public void testUnlockUserByLoginNotFound() {
        Assert.assertThrows(UserNotFoundException.class, () -> userService.unlockUserByLogin("meow"));
    }

    @Test
    public void testUpdateUser() {
        @NotNull final String firstName = "NEW_FIRST_NAME";
        @NotNull final String lastName = "NEW_LAST_NAME";
        @NotNull final String middleName = "NEW_MIDDLE_NAME";
        @Nullable final User user = userService.findByLogin("USER");
        Assert.assertNotNull(user);
        userService.updateUser(user.getId(), firstName, lastName, middleName);
        @Nullable final User updatedUser = userService.findByLogin("USER");
        Assert.assertNotNull(updatedUser);
        Assert.assertEquals(lastName, updatedUser.getLastName());
        Assert.assertEquals(firstName, updatedUser.getFirstName());
        Assert.assertEquals(middleName, updatedUser.getMiddleName());
    }

    @Test
    public void testUpdateUserIdNull() {
        Assert.assertThrows(IdEmptyException.class, () -> userService.updateUser(null, "FIRST_NAME", "LAST_NAME", "MIDDLE_NAME"));
    }

    @Test
    public void testUpdateUserIdEmpty() {
        Assert.assertThrows(IdEmptyException.class, () -> userService.updateUser("", "FIRST_NAME", "LAST_NAME", "MIDDLE_NAME"));
    }

    @Test
    public void testUpdateUserNotFound() {
        Assert.assertThrows(UserNotFoundException.class, () -> userService.updateUser("123321", "FIRST_NAME", "LAST_NAME", "MIDDLE_NAME"));
    }

}