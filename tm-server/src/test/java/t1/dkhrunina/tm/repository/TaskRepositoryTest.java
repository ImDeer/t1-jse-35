package t1.dkhrunina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import t1.dkhrunina.tm.api.repository.ITaskRepository;
import t1.dkhrunina.tm.enumerated.Sort;
import t1.dkhrunina.tm.exception.entity.EntityNotFoundException;
import t1.dkhrunina.tm.exception.field.IndexIncorrectException;
import t1.dkhrunina.tm.model.Task;
import t1.dkhrunina.tm.util.SystemUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TaskRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private final String USER_ID_1 = SystemUtil.generateGuid();

    @NotNull
    private final String USER_ID_2 = SystemUtil.generateGuid();

    @NotNull
    private final String PROJECT_ID_1 = SystemUtil.generateGuid();

    @NotNull
    private final String PROJECT_ID_2 = SystemUtil.generateGuid();

    @NotNull
    private List<Task> taskList;

    @NotNull
    private ITaskRepository taskRepository;

    @Before
    public void initRepository() {
        taskList = new ArrayList<>();
        taskRepository = new TaskRepository();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("task " + i);
            task.setDescription("description " + i);
            if (i <= 5) {
                task.setUserId(USER_ID_1);
                task.setProjectId(PROJECT_ID_1);
            } else {
                task.setUserId(USER_ID_2);
                task.setProjectId(PROJECT_ID_2);
            }
            taskRepository.add(task);
            taskList.add(task);
        }
    }

    @Test
    public void testAdd() {
        int expectedNumberOfEntries = taskRepository.getSize() + 1;
        @NotNull final String name = "Test Task";
        @NotNull final String description = "Test Description";
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(USER_ID_1, task);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
        @Nullable final Task createdTask = taskRepository.findOneByIndex(expectedNumberOfEntries - 1);
        Assert.assertNotNull(createdTask);
        Assert.assertEquals(USER_ID_1, createdTask.getUserId());
        Assert.assertEquals(name, createdTask.getName());
        Assert.assertEquals(description, createdTask.getDescription());
    }

    @Test
    public void testAddNull() {
        @Nullable final Task task = taskRepository.add(USER_ID_1, null);
        Assert.assertNull(task);
    }

    @Test
    public void testAddMultiple() {
        int expectedNumberOfEntries = taskRepository.getSize() + 2;
        @NotNull final List<Task> tasks = new ArrayList<>();
        @NotNull final String firstTaskName = "Test Task 1";
        @NotNull final String firstTaskDescription = "Test Description 1";
        @NotNull final Task firstTask = new Task();
        firstTask.setName(firstTaskName);
        firstTask.setDescription(firstTaskDescription);
        firstTask.setUserId(USER_ID_1);
        tasks.add(firstTask);
        @NotNull final String secondTaskName = "Test Task 2";
        @NotNull final String secondTaskDescription = "Test Description 2";
        @NotNull final Task secondTask = new Task();
        secondTask.setName(secondTaskName);
        secondTask.setDescription(secondTaskDescription);
        secondTask.setUserId(USER_ID_1);
        tasks.add(secondTask);
        taskRepository.add(tasks);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testClear() {
        int expectedNumberOfEntries = 0;
        taskRepository.clear();
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testClearForUser() {
        int expectedNumberOfEntries = 0;
        taskRepository.clear(USER_ID_1);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize(USER_ID_1));
    }

    @Test
    public void testExistByIdTrue() {
        @Nullable final Task task = taskRepository.findOneByIndex(1);
        Assert.assertNotNull(task);
        @Nullable final String taskId = task.getId();
        Assert.assertTrue(taskRepository.existsById(taskId));
    }

    @Test
    public void testExistByIdFalse() {
        Assert.assertFalse(taskRepository.existsById("123123"));
    }

    @Test
    public void testExistByIdForUserTrue() {
        @Nullable final Task task = taskRepository.findOneByIndex(USER_ID_1, 0);
        Assert.assertNotNull(task);
        @Nullable final String taskId = task.getId();
        Assert.assertTrue(taskRepository.existsById(USER_ID_1, taskId));
    }

    @Test
    public void testExistByIdForUserFalse() {
        Assert.assertFalse(taskRepository.existsById(USER_ID_1, "123123"));
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Task> tasks = taskRepository.findAll();
        Assert.assertEquals(taskList, tasks);
    }

    @Test
    @SuppressWarnings("unchecked assignment")
    public void testFindAllSortByName() {
        @NotNull Comparator<Task> comparator = Sort.BY_NAME.getComparator();
        @NotNull List<Task> tasks = taskRepository.findAll(comparator);
        taskList.sort(comparator);
        Assert.assertEquals(taskList, tasks);
    }

    @Test
    @SuppressWarnings("unchecked assignment")
    public void testFindAllSortByCreated() {
        @NotNull Comparator<Task> comparator = Sort.BY_CREATED.getComparator();
        @NotNull List<Task> tasks = taskRepository.findAll(comparator);
        taskList.sort(comparator);
        Assert.assertEquals(taskList, tasks);
    }

    @Test
    @SuppressWarnings("unchecked assignment")
    public void testFindAllSortByStatus() {
        @NotNull Comparator<Task> comparator = Sort.BY_STATUS.getComparator();
        @NotNull List<Task> tasks = taskRepository.findAll(comparator);
        taskList.sort(comparator);
        Assert.assertEquals(taskList, tasks);
    }

    @Test
    public void testFindAllForUser() {
        @NotNull List<Task> taskListForUser = taskList
                .stream()
                .filter(m -> USER_ID_1.equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull List<Task> tasks = taskRepository.findAll(USER_ID_1);
        Assert.assertEquals(taskListForUser, tasks);
    }

    @Test
    @SuppressWarnings("unchecked assignment")
    public void testFindAllForUserSortByName() {
        @NotNull List<Task> taskListForUser = taskList
                .stream()
                .filter(m -> USER_ID_1.equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull Comparator<Task> comparator = Sort.BY_NAME.getComparator();
        @NotNull List<Task> tasks = taskRepository.findAll(USER_ID_1, comparator);
        taskListForUser.sort(comparator);
        Assert.assertEquals(taskListForUser, tasks);
    }

    @Test
    @SuppressWarnings("unchecked assignment")
    public void testFindAllForUserSortByCreated() {
        @NotNull List<Task> taskListForUser = taskList
                .stream()
                .filter(m -> USER_ID_1.equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull Comparator<Task> comparator = Sort.BY_CREATED.getComparator();
        @NotNull List<Task> tasks = taskRepository.findAll(USER_ID_1, comparator);
        taskListForUser.sort(comparator);
        Assert.assertEquals(taskListForUser, tasks);
    }

    @Test
    @SuppressWarnings("unchecked assignment")
    public void testFindAllForUserSortByStatus() {
        @NotNull List<Task> taskListForUser = taskList
                .stream()
                .filter(m -> USER_ID_2.equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull Comparator<Task> comparator = Sort.BY_STATUS.getComparator();
        @NotNull List<Task> tasks = taskRepository.findAll(USER_ID_2, comparator);
        taskListForUser.sort(comparator);
        Assert.assertEquals(taskListForUser, tasks);
    }

    @Test
    public void testFindOneById() {
        @Nullable Task task;
        for (int i = 0; i < taskList.size(); i++) {
            task = taskRepository.findOneByIndex(i);
            Assert.assertNotNull(task);
            @NotNull final String taskId = task.getId();
            @Nullable final Task foundTask = taskRepository.findOneById(taskId);
            Assert.assertNotNull(foundTask);
        }
    }

    @Test
    public void testFindOneByNullId() {
        @Nullable final Task foundTask = taskRepository.findOneById("meow");
        Assert.assertNull(foundTask);
        Assert.assertThrows(EntityNotFoundException.class, () -> taskRepository.findOneById(""));
        Assert.assertThrows(EntityNotFoundException.class, () -> taskRepository.findOneById(null));
    }

    @Test
    public void testFindOneByIdForUser() {
        @Nullable Task task;
        @NotNull final List<Task> userTasks = taskList
                .stream()
                .filter(m -> USER_ID_1.equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 0; i < userTasks.size(); i++) {
            task = taskRepository.findOneByIndex(USER_ID_1, i);
            Assert.assertNotNull(task);
            @NotNull final String taskId = task.getId();
            @Nullable final Task foundTask = taskRepository.findOneById(USER_ID_1, taskId);
            Assert.assertNotNull(foundTask);
        }
    }

    @Test
    public void testFindOneByNullIdForUser() {
        @Nullable final Task foundTask = taskRepository.findOneById(USER_ID_1, "meow");
        Assert.assertNull(foundTask);
        Assert.assertThrows(EntityNotFoundException.class, () -> taskRepository.findOneById(USER_ID_1, ""));
        Assert.assertThrows(EntityNotFoundException.class, () -> taskRepository.findOneById(USER_ID_1, null));
    }

    @Test
    public void testFindOneByIndex() {
        for (int i = 0; i < taskList.size(); i++) {
            @Nullable final Task task = taskRepository.findOneByIndex(i);
            Assert.assertNotNull(task);
        }
    }

    @Test
    public void testFindOneByNullIndex() {
        Assert.assertThrows(IndexIncorrectException.class, () -> taskRepository.findOneByIndex(taskList.size()));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskRepository.findOneByIndex(-1));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskRepository.findOneByIndex(null));
    }

    @Test
    public void testFindOneByIndexForUser() {
        @NotNull final List<Task> userTasks = taskList
                .stream()
                .filter(m -> USER_ID_1.equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 0; i < userTasks.size(); i++) {
            @Nullable final Task task = taskRepository.findOneByIndex(USER_ID_1, i);
            Assert.assertNotNull(task);
        }
    }

    @Test
    public void testFindOneByNullIndexForUser() {
        Assert.assertThrows(IndexIncorrectException.class, () -> taskRepository.findOneByIndex(USER_ID_1, taskList.size()));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskRepository.findOneByIndex(USER_ID_1, -1));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskRepository.findOneByIndex(USER_ID_1, null));
    }

    @Test
    public void testGetSize() {
        int expectedSize = taskList.size();
        int actualSize = taskRepository.getSize();
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void testGetSizeForUser() {
        int expectedSize = (int) taskList
                .stream()
                .filter(m -> USER_ID_1.equals(m.getUserId()))
                .count();
        int actualSize = taskRepository.getSize(USER_ID_1);
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void testRemove() {
        @Nullable final Task task = taskRepository.findOneByIndex(0);
        Assert.assertNotNull(task);
        @NotNull final String taskId = task.getId();
        @Nullable final Task deletedTask = taskRepository.remove(task);
        Assert.assertNotNull(deletedTask);
        Assert.assertFalse(taskRepository.existsById(taskId));
    }

    @Test
    public void testRemoveNull() {
        Assert.assertThrows(EntityNotFoundException.class, () -> taskRepository.remove(null));
    }

    @Test
    public void testRemoveAllNull() {
        int expectedCount = taskRepository.getSize();
        taskRepository.removeAll(null);
        int actualCount = taskRepository.getSize();
        Assert.assertEquals(expectedCount, actualCount);
    }

    @Test
    public void testRemoveById() {
        int index = taskList.size();
        while (index > 0) {
            @Nullable final Task task = taskRepository.findOneByIndex(index - 1);
            Assert.assertNotNull(task);
            @NotNull final String taskId = task.getId();
            @Nullable final Task deletedTask = taskRepository.removeById(taskId);
            Assert.assertNotNull(deletedTask);
            Assert.assertFalse(taskRepository.existsById(taskId));
            index--;
        }
    }

    @Test
    public void testRemoveByNullId() {
        Assert.assertThrows(EntityNotFoundException.class, () -> taskRepository.removeById("meow"));
        Assert.assertThrows(EntityNotFoundException.class, () -> taskRepository.removeById(""));
        Assert.assertThrows(EntityNotFoundException.class, () -> taskRepository.removeById(null));
    }

    @Test
    public void testRemoveByIdForUser() {
        int index = (int) taskList
                .stream()
                .filter(m -> USER_ID_1.equals(m.getUserId()))
                .count();
        while (index > 0) {
            @Nullable final Task task = taskRepository.findOneByIndex(USER_ID_1, index - 1);
            Assert.assertNotNull(task);
            @NotNull final String taskId = task.getId();
            @Nullable final Task deletedTask = taskRepository.removeById(USER_ID_1, taskId);
            Assert.assertNotNull(deletedTask);
            Assert.assertFalse(taskRepository.existsById(USER_ID_1, taskId));
            index--;
        }
    }

    @Test
    public void testRemoveByNullIdForUser() {
        Assert.assertThrows(EntityNotFoundException.class, () -> taskRepository.removeById(USER_ID_1, "meow"));
        Assert.assertThrows(EntityNotFoundException.class, () -> taskRepository.removeById(USER_ID_1, ""));
        Assert.assertThrows(EntityNotFoundException.class, () -> taskRepository.removeById(USER_ID_1, null));
    }

    @Test
    public void testRemoveByIndex() {
        int index = taskList.size();
        while (index > 0) {
            @Nullable final Task deletedTask = taskRepository.removeByIndex(index - 1);
            Assert.assertNotNull(deletedTask);
            @NotNull final String taskId = deletedTask.getId();
            Assert.assertFalse(taskRepository.existsById(taskId));
            index--;
        }
    }

    @Test
    public void testRemoveByNullIndex() {
        Assert.assertThrows(IndexIncorrectException.class, () -> taskRepository.removeByIndex(-1));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskRepository.removeByIndex(taskRepository.getSize()));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskRepository.removeByIndex(null));
    }

    @Test
    public void testRemoveByIndexForUser() {
        int index = (int) taskList
                .stream()
                .filter(m -> USER_ID_1.equals(m.getUserId()))
                .count();
        while (index > 0) {
            @Nullable final Task deletedTask = taskRepository.removeByIndex(USER_ID_1, index - 1);
            Assert.assertNotNull(deletedTask);
            @NotNull final String taskId = deletedTask.getId();
            Assert.assertFalse(taskRepository.existsById(USER_ID_1, taskId));
            index--;
        }
    }

    @Test
    public void testRemoveByNullIndexForUser() {
        Assert.assertThrows(IndexIncorrectException.class, () -> taskRepository.removeByIndex(USER_ID_1, -1));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskRepository.removeByIndex(USER_ID_1, taskRepository.getSize()));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskRepository.removeByIndex(USER_ID_1, null));
    }

    @Test
    public void setTest() {
        int expectedNumberOfEntries = 2;
        @NotNull final List<Task> tasks = new ArrayList<>();
        @NotNull final String firstTaskName = "First Task Name";
        @NotNull final String firstTaskDescription = "Task Description";
        @NotNull final Task firstTask = new Task();
        firstTask.setName(firstTaskName);
        firstTask.setDescription(firstTaskDescription);
        tasks.add(firstTask);
        @NotNull final String secondTaskName = "Second Task Name";
        @NotNull final String secondTaskDescription = "Task Description";
        @NotNull final Task secondTask = new Task();
        secondTask.setName(secondTaskName);
        secondTask.setDescription(secondTaskDescription);
        tasks.add(secondTask);
        @NotNull final Collection<Task> addedTasks = taskRepository.set(tasks);
        Assert.assertTrue(addedTasks.size() > 0);
        int actualNumberOfEntries = taskRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

}
