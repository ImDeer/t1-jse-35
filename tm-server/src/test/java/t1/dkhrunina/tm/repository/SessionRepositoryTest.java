package t1.dkhrunina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import t1.dkhrunina.tm.api.repository.ISessionRepository;
import t1.dkhrunina.tm.exception.entity.EntityNotFoundException;
import t1.dkhrunina.tm.exception.field.IndexIncorrectException;
import t1.dkhrunina.tm.model.Session;
import t1.dkhrunina.tm.util.SystemUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class SessionRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private final String USER_ID_1 = SystemUtil.generateGuid();

    @NotNull
    private final String USER_ID_2 = SystemUtil.generateGuid();

    @NotNull
    private List<Session> sessionList;

    @NotNull
    private ISessionRepository sessionRepository;

    @Before
    public void initRepository() {
        sessionList = new ArrayList<>();
        sessionRepository = new SessionRepository();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Session session = new Session();
            if (i <= 5) session.setUserId(USER_ID_1);
            else session.setUserId(USER_ID_2);
            sessionRepository.add(session);
            sessionList.add(session);
        }
    }

    @Test
    public void testAdd() {
        int expectedNumberOfEntries = sessionRepository.getSize() + 1;
        @NotNull final Session session = new Session();
        sessionRepository.add(USER_ID_1, session);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
        @Nullable final Session createdSession = sessionRepository.findOneByIndex(expectedNumberOfEntries - 1);
        Assert.assertNotNull(createdSession);
        Assert.assertEquals(USER_ID_1, createdSession.getUserId());
    }

    @Test
    public void testAddNull() {
        @Nullable final Session session = sessionRepository.add(USER_ID_1, null);
        Assert.assertNull(session);
    }

    @Test
    public void testAddMultiple() {
        int expectedNumberOfEntries = sessionRepository.getSize() + 2;
        @NotNull final List<Session> sessions = new ArrayList<>();
        @NotNull final Session firstSession = new Session();
        firstSession.setUserId(USER_ID_1);
        sessions.add(firstSession);
        @NotNull final Session secondSession = new Session();
        secondSession.setUserId(USER_ID_2);
        sessions.add(secondSession);
        sessionRepository.add(sessions);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testClear() {
        int expectedNumberOfEntries = 0;
        sessionRepository.clear();
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testClearForUser() {
        int expectedNumberOfEntries = 0;
        sessionRepository.clear(USER_ID_1);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize(USER_ID_1));
    }

    @Test
    public void testExistByIdTrue() {
        @Nullable final Session session = sessionRepository.findOneByIndex(1);
        Assert.assertNotNull(session);
        @Nullable final String sessionId = session.getId();
        Assert.assertTrue(sessionRepository.existsById(sessionId));
    }

    @Test
    public void testExistByIdFalse() {
        Assert.assertFalse(sessionRepository.existsById("123123"));
    }

    @Test
    public void testExistByIdForUserTrue() {
        @Nullable final Session session = sessionRepository.findOneByIndex(USER_ID_1, 0);
        Assert.assertNotNull(session);
        @Nullable final String sessionId = session.getId();
        Assert.assertTrue(sessionRepository.existsById(USER_ID_1, sessionId));
    }

    @Test
    public void testExistByIdForUserFalse() {
        Assert.assertFalse(sessionRepository.existsById(USER_ID_1, "123123"));
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Session> sessions = sessionRepository.findAll();
        Assert.assertEquals(sessionList, sessions);
    }

    @Test
    public void testFindAllForUser() {
        @NotNull List<Session> sessionListForUser = sessionList
                .stream()
                .filter(m -> USER_ID_1.equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull List<Session> sessions = sessionRepository.findAll(USER_ID_1);
        Assert.assertEquals(sessionListForUser, sessions);
    }

    @Test
    public void testFindOneById() {
        @Nullable Session session;
        for (int i = 0; i < sessionList.size(); i++) {
            session = sessionRepository.findOneByIndex(i);
            Assert.assertNotNull(session);
            @NotNull final String sessionId = session.getId();
            @Nullable final Session foundSession = sessionRepository.findOneById(sessionId);
            Assert.assertNotNull(foundSession);
        }
    }

    @Test
    public void testFindOneByNullId() {
        @Nullable final Session foundSession = sessionRepository.findOneById("meow");
        Assert.assertNull(foundSession);
        Assert.assertThrows(EntityNotFoundException.class, () -> sessionRepository.findOneById(""));
        Assert.assertThrows(EntityNotFoundException.class, () -> sessionRepository.findOneById(null));
    }

    @Test
    public void testFindOneByIdForUser() {
        @Nullable Session session;
        @NotNull final List<Session> userSessions = sessionList
                .stream()
                .filter(m -> USER_ID_1.equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 0; i < userSessions.size(); i++) {
            session = sessionRepository.findOneByIndex(USER_ID_1, i);
            Assert.assertNotNull(session);
            @NotNull final String sessionId = session.getId();
            @Nullable final Session foundSession = sessionRepository.findOneById(USER_ID_1, sessionId);
            Assert.assertNotNull(foundSession);
        }
    }

    @Test
    public void testFindOneByNullIdForUser() {
        @Nullable final Session foundSession = sessionRepository.findOneById(USER_ID_1, "meow");
        Assert.assertNull(foundSession);
        Assert.assertThrows(EntityNotFoundException.class, () -> sessionRepository.findOneById(USER_ID_1, ""));
        Assert.assertThrows(EntityNotFoundException.class, () -> sessionRepository.findOneById(USER_ID_1, null));
    }

    @Test
    public void testFindOneByIndex() {
        for (int i = 0; i < sessionList.size(); i++) {
            @Nullable final Session session = sessionRepository.findOneByIndex(i);
            Assert.assertNotNull(session);
        }
    }

    @Test
    public void testFindOneByNullIndex() {
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionRepository.findOneByIndex(sessionList.size()));
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionRepository.findOneByIndex(-1));
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionRepository.findOneByIndex(null));
    }

    @Test
    public void testFindOneByIndexForUser() {
        @NotNull final List<Session> userSessions = sessionList
                .stream()
                .filter(m -> USER_ID_1.equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 0; i < userSessions.size(); i++) {
            @Nullable final Session session = sessionRepository.findOneByIndex(USER_ID_1, i);
            Assert.assertNotNull(session);
        }
    }

    @Test
    public void testFindOneByNullIndexForUser() {
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionRepository.findOneByIndex(USER_ID_1, sessionList.size()));
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionRepository.findOneByIndex(USER_ID_1, -1));
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionRepository.findOneByIndex(USER_ID_1, null));
    }

    @Test
    public void testGetSize() {
        int expectedSize = sessionList.size();
        int actualSize = sessionRepository.getSize();
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void testGetSizeForUser() {
        int expectedSize = (int) sessionList
                .stream()
                .filter(m -> USER_ID_1.equals(m.getUserId()))
                .count();
        int actualSize = sessionRepository.getSize(USER_ID_1);
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void testRemove() {
        @Nullable final Session session = sessionRepository.findOneByIndex(0);
        Assert.assertNotNull(session);
        @NotNull final String sessionId = session.getId();
        @Nullable final Session deletedSession = sessionRepository.remove(session);
        Assert.assertNotNull(deletedSession);
        Assert.assertFalse(sessionRepository.existsById(sessionId));
    }

    @Test
    public void testRemoveNull() {
        Assert.assertThrows(EntityNotFoundException.class, () -> sessionRepository.remove(null));
    }

    @Test
    public void testRemoveAllNull() {
        int expectedCount = sessionRepository.getSize();
        sessionRepository.removeAll(null);
        int actualCount = sessionRepository.getSize();
        Assert.assertEquals(expectedCount, actualCount);
    }

    @Test
    public void testRemoveById() {
        int index = sessionList.size();
        while (index > 0) {
            @Nullable final Session session = sessionRepository.findOneByIndex(index - 1);
            Assert.assertNotNull(session);
            @NotNull final String sessionId = session.getId();
            @Nullable final Session deletedSession = sessionRepository.removeById(sessionId);
            Assert.assertNotNull(deletedSession);
            Assert.assertFalse(sessionRepository.existsById(sessionId));
            index--;
        }
    }

    @Test
    public void testRemoveByNullId() {
        Assert.assertThrows(EntityNotFoundException.class, () -> sessionRepository.removeById("meow"));
        Assert.assertThrows(EntityNotFoundException.class, () -> sessionRepository.removeById(""));
        Assert.assertThrows(EntityNotFoundException.class, () -> sessionRepository.removeById(null));
    }

    @Test
    public void testRemoveByIdForUser() {
        int index = (int) sessionList
                .stream()
                .filter(m -> USER_ID_1.equals(m.getUserId()))
                .count();
        while (index > 0) {
            @Nullable final Session session = sessionRepository.findOneByIndex(USER_ID_1, index - 1);
            Assert.assertNotNull(session);
            @NotNull final String sessionId = session.getId();
            @Nullable final Session deletedSession = sessionRepository.removeById(USER_ID_1, sessionId);
            Assert.assertNotNull(deletedSession);
            Assert.assertFalse(sessionRepository.existsById(USER_ID_1, sessionId));
            index--;
        }
    }

    @Test
    public void testRemoveByNullIdForUser() {
        Assert.assertThrows(EntityNotFoundException.class, () -> sessionRepository.removeById(USER_ID_1, "meow"));
        Assert.assertThrows(EntityNotFoundException.class, () -> sessionRepository.removeById(USER_ID_1, ""));
        Assert.assertThrows(EntityNotFoundException.class, () -> sessionRepository.removeById(USER_ID_1, null));
    }

    @Test
    public void testRemoveByIndex() {
        int index = sessionList.size();
        while (index > 0) {
            @Nullable final Session deletedSession = sessionRepository.removeByIndex(index - 1);
            Assert.assertNotNull(deletedSession);
            @NotNull final String sessionId = deletedSession.getId();
            Assert.assertFalse(sessionRepository.existsById(sessionId));
            index--;
        }
    }

    @Test
    public void testRemoveByNullIndex() {
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionRepository.removeByIndex(-1));
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionRepository.removeByIndex(sessionRepository.getSize()));
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionRepository.removeByIndex(null));
    }

    @Test
    public void testRemoveByIndexForUser() {
        int index = (int) sessionList
                .stream()
                .filter(m -> USER_ID_1.equals(m.getUserId()))
                .count();
        while (index > 0) {
            @Nullable final Session deletedSession = sessionRepository.removeByIndex(USER_ID_1, index - 1);
            Assert.assertNotNull(deletedSession);
            @NotNull final String sessionId = deletedSession.getId();
            Assert.assertFalse(sessionRepository.existsById(USER_ID_1, sessionId));
            index--;
        }
    }

    @Test
    public void testRemoveByNullIndexForUser() {
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionRepository.removeByIndex(USER_ID_1, -1));
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionRepository.removeByIndex(USER_ID_1, sessionRepository.getSize()));
        Assert.assertThrows(IndexIncorrectException.class, () -> sessionRepository.removeByIndex(USER_ID_1, null));
    }

    @Test
    public void setTest() {
        int expectedNumberOfEntries = 2;
        @NotNull final List<Session> sessions = new ArrayList<>();
        @NotNull final Session firstSession = new Session();
        sessions.add(firstSession);
        @NotNull final Session secondSession = new Session();
        sessions.add(secondSession);
        @NotNull final Collection<Session> addedSessions = sessionRepository.set(sessions);
        Assert.assertTrue(addedSessions.size() > 0);
        int actualNumberOfEntries = sessionRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

}